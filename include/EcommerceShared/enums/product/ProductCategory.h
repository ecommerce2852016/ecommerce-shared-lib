//
// Created by federico on 24/02/24.
//

#ifndef ECOMMERCE_BE_PRODUCTCATEGORY_H
#define ECOMMERCE_BE_PRODUCTCATEGORY_H

#include "EcommerceShared/utils/json_struct.h"

JS_ENUM(ProductCategory, NONE, Electronics, Books, Clothing, Home, Garden, Beauty, Toys, Sports, Automotive, Music, Movies, Games, Tools, Grocery, Health, Jewelry, Baby, Software, Shoes, Handmade, Digital, Pet, Luggage, MusicalInstruments, OfficeProducts, Watches, Camera, VideoGames, Appliances, Furniture, FoodAndBeverages, ArtAndCrafts, Collectibles, Miscellaneous);

struct ProductCategoryData {
    ProductCategory productCategory;

    JS_OBJ(ProductCategory);
};
JS_ENUM_DECLARE_STRING_PARSER(ProductCategory)


class ProductCategoryConverter {
public:
    static std::string toString(ProductCategory productCategory) {
        switch (productCategory) {
            case ProductCategory::NONE:
                return "NONE";
            case ProductCategory::Electronics:
                return "Electronics";
            case ProductCategory::Books:
                return "Books";
            case ProductCategory::Clothing:
                return "Clothing";
            case ProductCategory::Home:
                return "Home";
            case ProductCategory::Garden:
                return "Garden";
            case ProductCategory::Beauty:
                return "Beauty";
            case ProductCategory::Toys:
                return "Toys";
            case ProductCategory::Sports:
                return "Sports";
            case ProductCategory::Automotive:
                return "Automotive";
            case ProductCategory::Music:
                return "Music";
            case ProductCategory::Movies:
                return "Movies";
            case ProductCategory::Games:
                return "Games";
            case ProductCategory::Tools:
                return "Tools";
            case ProductCategory::Grocery:
                return "Grocery";
            case ProductCategory::Health:
                return "Health";
            case ProductCategory::Jewelry:
                return "Jewelry";
            case ProductCategory::Baby:
                return "Baby";
            case ProductCategory::Software:
                return "Software";
            case ProductCategory::Shoes:
                return "Shoes";
            case ProductCategory::Handmade:
                return "Handmade";
            case ProductCategory::Digital:
                return "Digital";
            case ProductCategory::Pet:
                return "Pet";
            case ProductCategory::Luggage:
                return "Luggage";
            case ProductCategory::MusicalInstruments:
                return "MusicalInstruments";
            case ProductCategory::OfficeProducts:
                return "OfficeProducts";
            case ProductCategory::Watches:
                return "Watches";
            case ProductCategory::Camera:
                return "Camera";
            case ProductCategory::VideoGames:
                return "VideoGames";
            case ProductCategory::Appliances:
                return "Appliances";
            case ProductCategory::Furniture:
                return "Furniture";
            case ProductCategory::FoodAndBeverages:
                return "FoodAndBeverages";
            case ProductCategory::ArtAndCrafts:
                return "ArtAndCrafts";
            case ProductCategory::Collectibles:
                return "Collectibles";
            case ProductCategory::Miscellaneous:
                return "Miscellaneous";
            default:
                throw std::invalid_argument("[AccountType.h.toString] Invalid accountType");
        }
    }

    static ProductCategory fromString(const std::string &productCategory) {
        if (productCategory == "NONE") {
            return ProductCategory::NONE;
        } else if (productCategory == "Electronics") {
            return ProductCategory::Electronics;
        } else if (productCategory == "Books") {
            return ProductCategory::Books;
        } else if (productCategory == "Clothing") {
            return ProductCategory::Clothing;
        } else if (productCategory == "Home") {
            return ProductCategory::Home;
        } else if (productCategory == "Garden") {
            return ProductCategory::Garden;
        } else if (productCategory == "Beauty") {
            return ProductCategory::Beauty;
        } else if (productCategory == "Toys") {
            return ProductCategory::Toys;
        } else if (productCategory == "Sports") {
            return ProductCategory::Sports;
        } else if (productCategory == "Automotive") {
            return ProductCategory::Automotive;
        } else if (productCategory == "Music") {
            return ProductCategory::Music;
        } else if (productCategory == "Movies") {
            return ProductCategory::Movies;
        } else if (productCategory == "Games") {
            return ProductCategory::Games;
        } else if (productCategory == "Tools") {
            return ProductCategory::Tools;
        } else if (productCategory == "Grocery") {
            return ProductCategory::Grocery;
        } else if (productCategory == "Health") {
            return ProductCategory::Health;
        } else if (productCategory == "Jewelry") {
            return ProductCategory::Jewelry;
        } else if (productCategory == "Baby") {
            return ProductCategory::Baby;
        } else if (productCategory == "Software") {
            return ProductCategory::Software;
        } else if (productCategory == "Shoes") {
            return ProductCategory::Shoes;
        } else if (productCategory == "Handmade") {
            return ProductCategory::Handmade;
        } else if (productCategory == "Digital") {
            return ProductCategory::Digital;
        } else if (productCategory == "Pet") {
            return ProductCategory::Pet;
        } else if (productCategory == "Luggage") {
            return ProductCategory::Luggage;
        } else if (productCategory == "MusicalInstruments") {
            return ProductCategory::MusicalInstruments;
        } else if (productCategory == "OfficeProducts") {
            return ProductCategory::OfficeProducts;
        } else if (productCategory == "Watches") {
            return ProductCategory::Watches;
        } else if (productCategory == "Camera") {
            return ProductCategory::Camera;
        } else if (productCategory == "VideoGames") {
            return ProductCategory::VideoGames;
        } else if (productCategory == "Appliances") {
            return ProductCategory::Appliances;
        } else if (productCategory == "Furniture") {
            return ProductCategory::Furniture;
        } else if (productCategory == "FoodAndBeverages") {
            return ProductCategory::FoodAndBeverages;
        } else if (productCategory == "ArtAndCrafts") {
            return ProductCategory::ArtAndCrafts;
        } else if (productCategory == "Collectibles") {
            return ProductCategory::Collectibles;
        } else if (productCategory == "Miscellaneous") {
            return ProductCategory::Miscellaneous;
        } else {
            throw std::invalid_argument("[AccountType.h.fromString] Invalid accountType");
        }
    }
};

#endif //ECOMMERCE_BE_PRODUCTCATEGORY_H
