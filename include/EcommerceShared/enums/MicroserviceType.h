#ifndef ECOMMERCE_BE_MICROSERVICETYPE_H
#define ECOMMERCE_BE_MICROSERVICETYPE_H

#include <string>
#include <stdexcept>


// enum for all microservices of the application
enum MicroserviceType {
    api_gateway,
    user_ms,
    product_ms,
    orders_ms,
    payments_ms,
    delivery_ms,
};

class MicroserviceTypeConverter {
public:
    static std::string toString(MicroserviceType microserviceName) {
        switch (microserviceName) {
            case api_gateway:
                return "api_gateway";
            case user_ms:
                return "user_ms";
            case product_ms:
                return "product_ms";
            case orders_ms:
                return "orders_ms";
            case payments_ms:
                return "payments_ms";
            case delivery_ms:
                return "delivery_ms";
            default:
                throw std::invalid_argument("[MicroserviceType.h.toString] Invalid microserviceType");
        }
    }

    static MicroserviceType msTypeFromString(const std::string &microserviceName) {
        if (microserviceName == "api_gateway") {
            return api_gateway;
        } else if (microserviceName == "user_ms") {
            return user_ms;
        } else if (microserviceName == "product_ms") {
            return product_ms;
        } else if (microserviceName == "orders_ms") {
            return orders_ms;
        } else if (microserviceName == "payments_ms") {
            return payments_ms;
        } else if (microserviceName == "delivery_ms") {
            return delivery_ms;
        }
        throw std::invalid_argument("[MicroserviceType.h.msTypeFromString] Invalid microservice name");
    }
};

#endif //ECOMMERCE_BE_MICROSERVICETYPE_H
