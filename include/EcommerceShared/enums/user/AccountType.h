//
// Created by federico on 21/02/24.
//

#ifndef ECOMMERCE_BE_ACCOUNTTYPE_H
#define ECOMMERCE_BE_ACCOUNTTYPE_H

#include "EcommerceShared/utils/json_struct.h"

JS_ENUM(AccountType, ADMIN, CUSTOMER, VENDOR, TRANSPORTER)

struct AccountTypeData {
    AccountType accountType;

    JS_OBJ(accountType);
};
JS_ENUM_DECLARE_STRING_PARSER(AccountType)


class AccountTypeConverter {
public:
    static std::string toString(AccountType accountType) {
        switch (accountType) {
            case AccountType::ADMIN:
                return "ADMIN";
            case AccountType::CUSTOMER:
                return "CUSTOMER";
            case AccountType::VENDOR:
                return "VENDOR";
            case AccountType::TRANSPORTER:
                return "TRANSPORTER";
            default:
                throw std::invalid_argument("[AccountType.h.toString] Invalid accountType");
        }
    }

    static AccountType fromString(const std::string &accountType) {
        if (accountType == "ADMIN") {
            return AccountType::ADMIN;
        } else if (accountType == "CUSTOMER") {
            return AccountType::CUSTOMER;
        } else if (accountType == "VENDOR") {
            return AccountType::VENDOR;
        } else if (accountType == "TRANSPORTER") {
            return AccountType::TRANSPORTER;
        }
        throw std::invalid_argument("[AccountType.h.fromString] Invalid accountType");
    }
};


#endif //ECOMMERCE_BE_ACCOUNTTYPE_H



