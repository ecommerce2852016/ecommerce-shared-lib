#ifndef ECOMMERCE_BE_PAYMENTMETHOD_H
#define ECOMMERCE_BE_PAYMENTMETHOD_H

#include "EcommerceShared/utils/json_struct.h"

JS_ENUM(PaymentMethod, CREDIT_CARD, DEBIT_CARD, PAYPAL, STRIPE, VOUCHER)

struct PaymentMethodData {
    PaymentMethod paymentMethod;

    JS_OBJ(paymentMethod);
};
JS_ENUM_DECLARE_STRING_PARSER(PaymentMethod)

class PaymentMethodConverter {
public:
    static PaymentMethod fromString(const std::string &paymentMethodString) {
        if (paymentMethodString == "CREDIT_CARD") return PaymentMethod::CREDIT_CARD;
        if (paymentMethodString == "DEBIT_CARD") return PaymentMethod::DEBIT_CARD;
        if (paymentMethodString == "PAYPAL") return PaymentMethod::PAYPAL;
        if (paymentMethodString == "STRIPE") return PaymentMethod::STRIPE;
        if (paymentMethodString == "VOUCHER") return PaymentMethod::VOUCHER;
        throw std::invalid_argument("Invalid payment method string");
    }

    static std::string toString(const PaymentMethod &paymentMethod) {
        switch (paymentMethod) {
            case PaymentMethod::CREDIT_CARD:
                return "CREDIT_CARD";
            case PaymentMethod::DEBIT_CARD:
                return "DEBIT_CARD";
            case PaymentMethod::PAYPAL:
                return "PAYPAL";
            case PaymentMethod::STRIPE:
                return "STRIPE";
            case PaymentMethod::VOUCHER:
                return "VOUCHER";
        }
        throw std::invalid_argument("Invalid payment method");
    };
};

#endif //ECOMMERCE_BE_PAYMENTMETHOD_H
