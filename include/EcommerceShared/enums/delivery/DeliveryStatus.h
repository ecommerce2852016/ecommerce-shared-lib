//
// Created by federico on 28/02/24.
//

#ifndef ECOMMERCE_BE_DELIVERYSTATUS_H
#define ECOMMERCE_BE_DELIVERYSTATUS_H


#include "EcommerceShared/utils/json_struct.h"

JS_ENUM(DeliveryStatus, NONE, AWAITING_PICKUP, IN_TRANSIT, DELIVERED, CANCELLED)

struct DeliveryStatusData {
    DeliveryStatus deliveryStatus;

    JS_OBJ(deliveryStatus);
};
JS_ENUM_DECLARE_STRING_PARSER(DeliveryStatus)

class DeliveryStatusConverter {
public:
    static std::string toString(DeliveryStatus deliveryStatus) {
        switch (deliveryStatus) {
            case DeliveryStatus::NONE:
                return "NONE";
            case DeliveryStatus::AWAITING_PICKUP:
                return "AWAITING_PICKUP";
            case DeliveryStatus::IN_TRANSIT:
                return "IN_TRANSIT";
            case DeliveryStatus::DELIVERED:
                return "DELIVERED";
            case DeliveryStatus::CANCELLED:
                return "CANCELLED";
            default:
                throw std::invalid_argument("[DeliveryStatus.h.toString] Invalid deliveryStatus");
        };
    }

    static DeliveryStatus fromString(const std::string &deliveryStatus) {
        if (deliveryStatus == "NONE") {
            return DeliveryStatus::NONE;
        } else if (deliveryStatus == "AWAITING_PICKUP") {
            return DeliveryStatus::AWAITING_PICKUP;
        } else if (deliveryStatus == "IN_TRANSIT") {
            return DeliveryStatus::IN_TRANSIT;
        } else if (deliveryStatus == "DELIVERED") {
            return DeliveryStatus::DELIVERED;
        } else if (deliveryStatus == "CANCELLED") {
            return DeliveryStatus::CANCELLED;
        } else {
            throw std::invalid_argument("[DeliveryStatus.h.fromString] Invalid deliveryStatus");
        }
    }
};

#endif //ECOMMERCE_BE_DELIVERYSTATUS_H
