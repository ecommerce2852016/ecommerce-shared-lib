#ifndef ECOMMERCE_BE_DELIVERYTYPE_H
#define ECOMMERCE_BE_DELIVERYTYPE_H


#include "EcommerceShared/utils/json_struct.h"

JS_ENUM(DeliveryType, STANDARD, EXPRESS, PICKUP, CUSTOM, OTHER)

struct DeliveryTypeData {
    DeliveryType deliveryType;


    static float getPriceForDeliveryType(DeliveryType type) {
        switch (type) {
            case DeliveryType::STANDARD:
                return 3;
            case DeliveryType::EXPRESS:
                return 10;
            case DeliveryType::PICKUP:
                return 5;
            case DeliveryType::CUSTOM:
                return 6;
            case DeliveryType::OTHER:
                return 8;
        }
        throw std::invalid_argument("Invalid enum value for DeliveryType");
    }

    JS_OBJ(deliveryType);
};
JS_ENUM_DECLARE_STRING_PARSER(DeliveryType)

// function to get price for delivery type TODO use table instead of switch



class DeliveryTypeConverter {
public:
    static DeliveryType fromString(const std::string &str) {
        if (str == "STANDARD") return DeliveryType::STANDARD;
        if (str == "EXPRESS") return DeliveryType::EXPRESS;
        if (str == "PICKUP") return DeliveryType::PICKUP;
        if (str == "CUSTOM") return DeliveryType::CUSTOM;
        if (str == "OTHER") return DeliveryType::OTHER;
        throw std::invalid_argument("Invalid string value for DeliveryType");
    }

    static std::string toString(DeliveryType type) {
        switch (type) {
            case DeliveryType::STANDARD:
                return "STANDARD";
            case DeliveryType::EXPRESS:
                return "EXPRESS";
            case DeliveryType::PICKUP:
                return "PICKUP";
            case DeliveryType::CUSTOM:
                return "CUSTOM";
            case DeliveryType::OTHER:
                return "OTHER";
        }
        throw std::invalid_argument("Invalid enum value for DeliveryType");
    }
};

#endif //ECOMMERCE_BE_DELIVERYTYPE_H
