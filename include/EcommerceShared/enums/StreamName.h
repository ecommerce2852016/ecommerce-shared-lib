//
// Created by federico on 14/02/24.
//

#ifndef ECOMMERCE_BE_STREAMNAME_H
#define ECOMMERCE_BE_STREAMNAME_H

#include <string>
#include <stdexcept>

// enum for all stream used by the the application
enum StreamName {
    ecommerce_stream,
};

class StreamNameConverter {
public:
    static std::string toString(StreamName streamName) {
        switch (streamName) {
            case ecommerce_stream:
                return "ecommerce_stream";
            default:
                throw std::invalid_argument("[StreamName.h.toString] Invalid stream");
        }
    }

    static StreamName streamNameFromString(const std::string &streamName) {
        if (streamName == "ecommerce_stream") {
            return ecommerce_stream;
        }
        throw std::invalid_argument("[StreamName.h.msTypeFromString] Invalid stream name");
    }
};


#endif //ECOMMERCE_BE_STREAMNAME_H
