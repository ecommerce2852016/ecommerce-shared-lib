#ifndef ECOMMERCE_BE_SHIPMENTDTO_H
#define ECOMMERCE_BE_SHIPMENTDTO_H

#include <string>
#include <optional>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/ControllerUtils.h"

namespace ecommerce::dto {

    struct ShipmentDTO {
        std::optional<long> id;
        std::optional<long> orderId;
        std::string trackingCode;
        std::string created_at;
        std::string externalId;
        std::string external_api_type;
        std::optional<std::string> status;
        std::optional<long> fromAddressId;
        std::optional<long> toAddressId;
        std::optional<std::string> service;
        std::optional<std::string> carrier_account_id;

        JS_OBJ(id, orderId, trackingCode, created_at, externalId, external_api_type, status, fromAddressId, toAddressId, service, carrier_account_id);
    };


}
#endif //ECOMMERCE_BE_SHIPMENTDTO_H
