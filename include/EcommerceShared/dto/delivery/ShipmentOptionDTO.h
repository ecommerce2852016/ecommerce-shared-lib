#ifndef ECOMMERCE_BE_SHIPMENTOPTIONDTO_H
#define ECOMMERCE_BE_SHIPMENTOPTIONDTO_H

#include <string>
#include "EcommerceShared/utils/json_struct.h"

namespace ecommerce::dto {

    struct ShipmentOptionDTO {
        long fromAddressId;
        std::string carrier_account_id;
        std::string service;

        JS_OBJ(fromAddressId, carrier_account_id, service);

        void executeValidityCheck() const {
            if (fromAddressId < 0) throw std::invalid_argument("fromAddressId must be non-negative");
            if (carrier_account_id.empty()) throw std::invalid_argument("carrier_account_id must not be empty");
            if (service.empty()) throw std::invalid_argument("service must not be empty");
        }

    };

}
#endif //ECOMMERCE_BE_SHIPMENTOPTIONDTO_H
