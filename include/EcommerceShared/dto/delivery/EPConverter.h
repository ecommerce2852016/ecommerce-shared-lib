#ifndef ECOMMERCE_BE_EPCONVERTER_H
#define ECOMMERCE_BE_EPCONVERTER_H

#include "EPShippingData.h"
#include "AddressDTO.h"
#include "EcommerceShared/dto/user/UserAccountDTO.h"

using namespace ecommerce::dto;

/**
 * Converter for EasyPost objects
 */
class EPConverter {
public:

    static EPAddressDTO EPAddressfromAddressDTO(const AddressDTO &addressDTO, const UserAccountDTO &userAccountDTO) {
        EPAddressDTO epAddressDTO;
        epAddressDTO.verify = true;
        epAddressDTO.street1 = addressDTO.street;
        epAddressDTO.city = addressDTO.city;
        epAddressDTO.zip = addressDTO.zipCode;
        epAddressDTO.country = addressDTO.country;
        epAddressDTO.phone = addressDTO.phone;
        epAddressDTO.name = userAccountDTO.fullName;
        return epAddressDTO;
    }

};

#endif //ECOMMERCE_BE_EPCONVERTER_H
