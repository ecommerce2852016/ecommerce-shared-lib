#ifndef ECOMMERCE_BE_ADDRESSDTO_H
#define ECOMMERCE_BE_ADDRESSDTO_H

#include <string>
#include <optional>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/ControllerUtils.h"

namespace ecommerce::dto {
    struct AddressDTO {
        std::optional<long> id;
        std::string street;
        std::string addressType;
        std::optional<long> userAccountId;
        std::string city;
        std::string zipCode;
        std::string country;
        std::string phone;
        std::optional<std::string> externalId;
        std::optional<std::string> externalApiType;
        bool externalValidity = false;

        JS_OBJ(id, street, addressType, userAccountId, city, zipCode, country, phone, externalId, externalApiType, externalValidity);

        void executeValidityCheck() const {
            if (street.empty()) throw std::invalid_argument("street must not be empty");
            if (addressType.empty()) throw std::invalid_argument("addressType must not be empty");
            if (!userAccountId.has_value()) throw std::invalid_argument("userAccountId must not be empty");
            if (city.empty()) throw std::invalid_argument("city must not be empty");
            if (zipCode.empty()) throw std::invalid_argument("zipCode must not be empty");
            if (country.empty()) throw std::invalid_argument("country must not be empty");
            if (phone.empty()) throw std::invalid_argument("phone must not be empty");
            if (country.size() != 2) throw std::invalid_argument("country must be in ISO 3166-1 alpha-2 format");
            if (zipCode.size() < 3) throw std::invalid_argument("zipCode must be at least 3 characters long");

        }
    };

}


// request body controller auto converters
namespace drogon {
    template<>
    inline ecommerce::dto::AddressDTO fromRequest(const HttpRequest &req) {
        return ControllerUtils::parseRequestBody<ecommerce::dto::AddressDTO>(req);
    }
}


#endif //ECOMMERCE_BE_ADDRESSDTO_H
