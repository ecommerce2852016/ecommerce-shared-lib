#ifndef ECOMMERCE_BE_RETRIEVERATESFORSHIPMENTREQUESTDTO_H
#define ECOMMERCE_BE_RETRIEVERATESFORSHIPMENTREQUESTDTO_H

#include <string>
#include <optional>
#include "EcommerceShared/utils/json_struct.h"
#include "AddressDTO.h"
#include "ShipmentDTO.h"
#include "Shipment.h"

namespace ecommerce::dto {
    struct RetrieveRatesForShipmentRequestDTO {
        std::optional<drogon_model::ecommerce_deliveries::Shipment> shipment;
        std::optional<ShipmentDTO> shipmentDTO;
        AddressDTO fromAddress;
        AddressDTO toAddress;
        UserAccountDTO fromUser;
        UserAccountDTO toUser;
        double weight = 0;

        JS_OBJ(shipment, fromAddress, toAddress, fromUser, toUser, weight);
    };


}

#endif //ECOMMERCE_BE_RETRIEVERATESFORSHIPMENTREQUESTDTO_H
