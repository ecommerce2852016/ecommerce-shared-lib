#ifndef ECOMMERCE_BE_MOCKSHIPPINGDATA_H
#define ECOMMERCE_BE_MOCKSHIPPINGDATA_H

#include <string>
#include <optional>
#include <vector>
#include "EcommerceShared/utils/json_struct.h"

namespace ecommerce::dto {

    struct MockAddressDTO {
        std::optional<bool> verify;
        std::optional<bool> verify_strict;
        std::optional<std::string> object;
        std::string name;
        std::string street1;
        std::string city;
        std::string state;
        std::string zip;
        std::string country;
        std::string phone;
        std::string email;

        JS_OBJ(verify, verify_strict, object, name, street1, city, state, zip, country, phone, email);
    };

    struct MockRateDTO {
        std::string object;
        std::string mode;
        std::string service;
        std::string carrier;
        std::optional<double> rate;
        std::string currency;
        JS::Nullable<int> delivery_days;
        std::string delivery_date;
        JS::Nullable<bool> delivery_date_guaranteed;
        std::string est_delivery_date;
        std::string carrier_account_id;

        JS_OBJ(object, mode, service, carrier, rate, currency, delivery_days, delivery_date, delivery_date_guaranteed, est_delivery_date, carrier_account_id);
    };

    struct MockShipmentDTO {
        std::optional<std::string> id;
        std::optional<std::string> status;
        std::optional<std::string> tracking_code;
        std::optional<std::string> updated_at;
        std::optional<std::string> service;
        std::optional<std::vector<std::string>> carrier_accounts;
        std::optional<MockRateDTO> selected_rate;
        MockAddressDTO from_address;
        MockAddressDTO to_address;

        JS_OBJ(id, status, tracking_code, updated_at, service, carrier_accounts, selected_rate, from_address, to_address);
    };



}

#endif //ECOMMERCE_BE_MOCKSHIPPINGDATA_H
