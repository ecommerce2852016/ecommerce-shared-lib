#ifndef ECOMMERCE_BE_EPSHIPPINGDATA_H
#define ECOMMERCE_BE_EPSHIPPINGDATA_H

#include <string>
#include <vector>
#include <optional>
#include "EcommerceShared/utils/json_struct.h"

namespace ecommerce::dto {

    struct EPDetailsDTO {
        double latitude;
        double longitude;
        std::string time_zone;

        JS_OBJ(latitude, longitude, time_zone);
    };

    struct EPVerificationDTO {
        std::optional<bool> success;
        std::vector<std::string> errors;
        std::optional<EPDetailsDTO> details;

        JS_OBJ(success, errors, details);
    };

    struct EPAddressDTO {
        std::optional<bool> verify;
        std::optional<bool> verify_strict;
        std::optional<std::string> object;
        std::string name;
        std::string street1;
        std::string city;
        std::string state;
        std::string zip;
        std::string country;
        std::string phone;
        std::string email;
        std::optional<EPVerificationDTO> verifications;

        JS_OBJ(verify, verify_strict, object, name, street1, city, state, zip, country, phone, email, verifications);
    };

    struct EPRateDTO {
        std::string object;
        std::string mode;
        std::string service;
        std::string carrier;
        std::optional<double> rate;
        std::string currency;
        JS::Nullable<int> delivery_days;
        std::string delivery_date;
        JS::Nullable<bool> delivery_date_guaranteed;
        std::string est_delivery_date;
        std::string carrier_account_id;

        JS_OBJ(object, mode, service, carrier, rate, currency, delivery_days, delivery_date, delivery_date_guaranteed, est_delivery_date, carrier_account_id);
    };

    struct EPPaymentDTO {
        std::string type;

        JS_OBJ(type);
    };

    struct EPOptionsDTO {
        std::string currency;
        std::optional<EPPaymentDTO> payment;
        std::optional<int> date_advance;

        JS_OBJ(currency, payment, date_advance);
    };

    struct EPParcelDTO {
        std::optional<std::string> object;
        double weight;

        JS_OBJ(object, weight);
    };

    struct EPShippingDataDTO {
        EPAddressDTO from_address;
        EPAddressDTO to_address;
        std::vector<EPRateDTO> rates;
        EPOptionsDTO options;
        EPParcelDTO parcel;
        std::optional<std::vector<std::string>> messages;

        JS_OBJ(from_address, to_address, rates, options, parcel, messages);
    };

    struct EPTrackerDTO {
        std::string id;
        std::string object;
        std::string mode;
        std::string tracking_code;
        std::string status;
        std::string status_detail;
        std::string created_at;
        std::string updated_at;
        std::optional<std::string> signed_by;
        JS::Nullable<double> weight;
        std::optional<std::string> est_delivery_date;
        std::string shipment_id;
        std::string carrier;
        std::vector<std::string> tracking_details;
        std::vector<std::string> fees;
        std::optional<std::string> carrier_detail;
        std::string public_url;

        JS_OBJ(id, object, mode, tracking_code, status, status_detail, created_at, updated_at, signed_by, weight, est_delivery_date, shipment_id, carrier, tracking_details, fees, carrier_detail, public_url);
    };

    struct EPFeeDTO {
        std::string object;
        std::string type;
        double amount;
        std::optional<bool> charged;
        std::optional<bool> refunded;

        JS_OBJ(object, type, amount, charged, refunded);
    };

    struct EPPostageLabelDTO {
        std::string object;
        std::string id;
        std::string created_at;
        std::string updated_at;
        int date_advance;
        std::string integrated_form;
        std::string label_date;
        int label_resolution;
        std::string label_size;
        std::string label_type;
        std::string label_file_type;
        std::optional<std::string> label_url;
        std::optional<std::string> label_pdf_url;
        std::optional<std::string> label_zpl_url;
        std::optional<std::string> label_epl2_url;
        std::optional<std::string> label_file;

        JS_OBJ(object, id, created_at, updated_at, date_advance, integrated_form, label_date, label_resolution, label_size, label_type, label_file_type, label_url, label_pdf_url, label_zpl_url, label_epl2_url, label_file);
    };


    struct EPShipmentDTO {
        std::optional<std::string> created_at;
        std::optional<bool> is_return;
        std::optional<std::vector<std::string>> messages;
        std::optional<std::string> mode;
        EPOptionsDTO options;
        std::optional<std::string> reference;
        std::optional<std::string> status;
        std::optional<std::string> tracking_code;
        std::optional<std::string> updated_at;
        std::optional<std::string> batch_id;
        std::optional<std::string> batch_status;
        std::optional<std::string> batch_message;
        std::optional<std::string> customs_info;
        EPAddressDTO from_address;
        std::optional<std::string> insurance;
        std::optional<std::string> order_id;
        EPParcelDTO parcel;
        std::optional<EPPostageLabelDTO> postage_label;
        std::optional<std::vector<EPRateDTO>> rates;
        std::optional<std::string> refund_status;
        std::optional<std::string> scan_form;
        std::optional<EPRateDTO> selected_rate;
        std::optional<EPTrackerDTO> tracker;
        EPAddressDTO to_address;
        std::optional<std::string> usps_zone;
        std::optional<EPAddressDTO> return_address;
        std::optional<EPAddressDTO> buyer_address;
        std::optional<std::vector<std::string>> forms;
        std::optional<std::vector<EPFeeDTO>> fees;
        std::optional<std::string> id;
        std::optional<std::string> object;
        std::optional<std::string> service;
        std::optional<std::vector<std::string>> carrier_accounts;

        JS_OBJ(created_at, is_return, messages, mode, options, reference, status, tracking_code, updated_at, batch_id, batch_status, batch_message, customs_info, from_address, insurance, order_id, parcel, postage_label, rates, refund_status, scan_form, selected_rate, tracker, to_address, usps_zone, return_address, buyer_address, forms, fees, id, object, service, carrier_accounts);
    };

    struct ShipmentRequestDTO {
        EPShipmentDTO shipment;

        JS_OBJ(shipment);
    };

}

#endif //ECOMMERCE_BE_EPSHIPPINGDATA_H
