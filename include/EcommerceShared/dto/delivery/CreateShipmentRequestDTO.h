
#ifndef ECOMMERCE_BE_CREATESHIPMENTREQUESTDTO_H
#define ECOMMERCE_BE_CREATESHIPMENTREQUESTDTO_H

#include <string>
#include "EcommerceShared/utils/json_struct.h"
#include "ShipmentOptionDTO.h"
#include "EcommerceShared/utils/VectorUtils.h"

namespace ecommerce::dto {

    struct CreateShipmentRequestDTO {
        std::optional<long> fromAddressId;
        std::optional<long> toAddressId;
        std::optional<std::string> carrier_account_id;
        std::optional<std::string> service;
        std::optional<double> weight;
        std::optional<long> orderId;

        JS_OBJ(fromAddressId, toAddressId, carrier_account_id, service, weight, orderId);

        void executeValidityCheck() const {
            if (!fromAddressId.has_value()) throw std::invalid_argument("fromAddressId cannot be empty");
            if (!toAddressId.has_value()) throw std::invalid_argument("toAddressId cannot be empty");
            if (!carrier_account_id.has_value()) throw std::invalid_argument("carrier_account_id cannot be empty");
            if (!service.has_value()) throw std::invalid_argument("service cannot be empty");
            if (!weight.has_value()) throw std::invalid_argument("weight cannot be empty");
            if (!orderId.has_value()) throw std::invalid_argument("orderId cannot be empty");
        }

    };

}

#endif //ECOMMERCE_BE_CREATESHIPMENTREQUESTDTO_H
