#ifndef ECOMMERCE_BE_SHIPMENTPREVIEWDTO_H
#define ECOMMERCE_BE_SHIPMENTPREVIEWDTO_H


#include <string>
#include <optional>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/ControllerUtils.h"
#include "EcommerceShared/dto/product/CartItemDTO.h"
#include "MockShippingData.h"

namespace ecommerce::dto {

    struct ShipmentPreviewDTO {
        long fromAddressId;
        std::vector<CartItemDTO> items;
        vector<MockRateDTO> rates;

        JS_OBJ(fromAddressId, items, rates);
    };

}
#endif //ECOMMERCE_BE_SHIPMENTPREVIEWDTO_H
