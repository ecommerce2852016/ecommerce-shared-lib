#ifndef ECOMMERCE_BE_SHIPMENTPREVIEWREQUESTDTO_H
#define ECOMMERCE_BE_SHIPMENTPREVIEWREQUESTDTO_H


#include <string>
#include <optional>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/ControllerUtils.h"
#include "EcommerceShared/dto/product/CartItemDTO.h"

namespace ecommerce::dto {

    struct ShipmentPreviewRequestDTO {
        long toAddressId;
        vector<CartItemDTO> items;

        JS_OBJ(toAddressId, items);

        void executeValidityCheck() const {
            if (toAddressId < 0) throw std::invalid_argument("toAddressId must be non-negative");
            if (items.empty()) throw std::invalid_argument("items must not be empty for shipment preview");
        }
    };

}


#endif //ECOMMERCE_BE_SHIPMENTPREVIEWREQUESTDTO_H
