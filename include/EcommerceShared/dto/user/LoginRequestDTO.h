//
// Created by federico on 21/02/24.
//

#ifndef ECOMMERCE_BE_LOGINREQUESTDTO_H
#define ECOMMERCE_BE_LOGINREQUESTDTO_H

#include <string>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/ControllerUtils.h"

namespace ecommerce::dto {
    struct LoginRequestDTO {
        std::string username;
        std::string password;

        void executeValidityCheck() const {
            if (username.empty() || password.empty()) {
                throw std::invalid_argument("Username and password must not be empty");
            }
        }

        JS_OBJ(username, password);
    };
}


// request body controller auto converters
namespace drogon {
    template<>
    inline ecommerce::dto::LoginRequestDTO fromRequest(const HttpRequest &req) {
        return ControllerUtils::parseRequestBody<ecommerce::dto::LoginRequestDTO>(req);
    }
}

#endif //ECOMMERCE_BE_LOGINREQUESTDTO_H
