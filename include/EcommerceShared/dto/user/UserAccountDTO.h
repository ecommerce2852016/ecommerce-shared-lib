//
// Created by federico on 15/02/24.
//

#ifndef ECOMMERCE_BE_USERACCOUNTDTO_H
#define ECOMMERCE_BE_USERACCOUNTDTO_H

#include "string"
#include "EcommerceShared/enums/user/AccountType.h"

namespace ecommerce::dto {


    struct UserAccountDTO {
        long id;
        std::string username;
        std::string email;
        std::string createdAt;
        AccountType accountType;
        std::string taxCode;
        std::string partitaIva;
        std::string firstName;
        std::string lastName;
        std::string fullName;

        JS_OBJ(id, username, email, createdAt, accountType, taxCode, partitaIva, firstName, lastName, fullName);
    };
}
#endif //ECOMMERCE_BE_USERACCOUNTDTO_H
