//
// Created by federico on 21/02/24.
//

#ifndef ECOMMERCE_BE_LOGINRESPONSEDTO_H
#define ECOMMERCE_BE_LOGINRESPONSEDTO_H

#include <string>
#include <EcommerceShared/utils/json_struct.h>

struct LoginResponseDTO {
    std::string token;
    std::string tokenType;
    long expiresIn;
    AccountType accountType;

    JS_OBJ(token, tokenType, expiresIn, accountType);
};
#endif //ECOMMERCE_BE_LOGINRESPONSEDTO_H
