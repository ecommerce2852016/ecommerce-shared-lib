#ifndef ECOMMERCE_BE_SIGNUPUSERDTO_H
#define ECOMMERCE_BE_SIGNUPUSERDTO_H

#include "string"
#include <drogon/HttpRequest.h>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/ControllerUtils.h"
#include "EcommerceShared/exceptions/http_exception.h"
#include "EcommerceShared/enums/user/AccountType.h"

using namespace std;
using namespace drogon;
using namespace ecommerce::exceptions;


namespace ecommerce::dto {


    struct SignupUserAccountDTO {
        std::string username;
        std::string email;
        std::string password;
        AccountType accountType;

        // customer
        std::string taxCode;
        std::string firstName;
        std::string lastName;

        // vendor and trasporter
        std::string partitaIva;
        std::string fullName;

        std::string ipAddress;


        JS_OBJ(username, email, password, accountType, taxCode, firstName, lastName, partitaIva, fullName, ipAddress);


        void executeValidityCheck() const {
            if (username.empty() || email.empty() || password.empty()) {
                throw http_exception("username, email, password cannot be empty", drogon::k400BadRequest);
            }
            if (accountType != AccountType::CUSTOMER && accountType != AccountType::VENDOR && accountType != AccountType::ADMIN && accountType != AccountType::TRANSPORTER) {
                throw http_exception("wrong accountType", drogon::k400BadRequest);
            }

            if (accountType == AccountType::CUSTOMER) {
                if (firstName.empty() || lastName.empty() || taxCode.empty()) {
                    throw http_exception("account type CUSTOMER must have: firstName, lastName, taxCode", drogon::k400BadRequest);
                }
            }

            if (accountType == AccountType::VENDOR) {
                if (partitaIva.empty() || fullName.empty()) {
                    throw http_exception("account type VENDOR must have: partitaIva, fullName", drogon::k400BadRequest);
                }
            }

            if (accountType == AccountType::TRANSPORTER) {
                if (partitaIva.empty() || fullName.empty()) {
                    throw http_exception("account type TRANSPORTER must have: partitaIva, fullName", drogon::k400BadRequest);
                }
            }
        }
    };


}


// request body controller auto converters
namespace drogon {
    template<>
    inline ecommerce::dto::SignupUserAccountDTO fromRequest(const HttpRequest &req) {
        return ControllerUtils::parseRequestBody<ecommerce::dto::SignupUserAccountDTO>(req);
    }
}


#endif //ECOMMERCE_BE_SIGNUPUSERDTO_H
