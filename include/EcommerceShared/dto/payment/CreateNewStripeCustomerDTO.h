#ifndef ECOMMERCE_BE_CREATENEWSTRIPECUSTOMERDTO_H
#define ECOMMERCE_BE_CREATENEWSTRIPECUSTOMERDTO_H

#include <string>
#include <vector>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/enums/delivery/DeliveryType.h"
#include "EcommerceShared/enums/payment/PaymentMethod.h"


namespace ecommerce::dto {

    struct CreateNewStripeCustomerDTO {
        std::optional<long> customerId;
        std::optional<std::string> name;
        std::optional<std::string> email;

        JS_OBJ(customerId, name, email);

        void executeValidityCheck() const{
            if (!customerId.has_value()) throw std::invalid_argument("customerId must not be empty");
            if (!name.has_value()) throw std::invalid_argument("name must not be empty");
        }
    };
}

#endif //ECOMMERCE_BE_CREATENEWSTRIPECUSTOMERDTO_H
