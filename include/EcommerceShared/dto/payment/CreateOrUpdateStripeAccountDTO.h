#ifndef ECOMMERCE_BE_CREATEORUPDATESTRIPEACCOUNTDTO_H
#define ECOMMERCE_BE_CREATEORUPDATESTRIPEACCOUNTDTO_H


#include <string>
#include <vector>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/DateUtils.h"
#include "EcommerceShared/enums/delivery/DeliveryType.h"
#include "EcommerceShared/enums/payment/PaymentMethod.h"


namespace ecommerce::dto {

    struct CreateOrUpdateStripeAccountDTO {
        std::optional<long> userAccountId;
        std::optional<std::string> email;
        std::optional<std::string> firstName;
        std::optional<std::string> lastName;
        std::optional<std::string> fullName;
        std::optional<std::string> birthDate;
        std::optional<std::string> phone;

        // address
        std::optional<std::string> city;
        std::optional<std::string> postalCode;
        std::optional<std::string> line;

        std::optional<bool> tosAcceptance;
        std::optional<std::string> tosAcceptanceIpAddress;

        JS_OBJ(userAccountId, email, firstName, lastName, fullName, birthDate, phone, city, postalCode, line, tosAcceptance, tosAcceptanceIpAddress);

        void executeValidityCheck() const {
            if (!userAccountId.has_value()) throw std::invalid_argument("userAccountId must not be empty");
        }

        std::string getAsRequestBodyForm() const {
            std::string body = "";
            if (city.has_value()) body += "&individual[address][city]=" + city.value();
            if (line.has_value()) body += "&individual[address][line1]=" + line.value();
            if (postalCode.has_value()) body += "&individual[address][postal_code]=" + postalCode.value();
            if (email.has_value()) body += "&individual[email]=" + email.value();
            if (firstName.has_value()) body += "&individual[first_name]=" + firstName.value();
            if (lastName.has_value()) body += "&individual[last_name]=" + lastName.value();
            if (phone.has_value()) body += "&individual[phone]=" + phone.value();
            auto timestamp = std::chrono::system_clock::now();
            auto seconds_since_epoch = std::chrono::duration_cast<std::chrono::seconds>(timestamp.time_since_epoch()).count();
            if (tosAcceptance.has_value()) body += "&tos_acceptance[date]=" + std::to_string(seconds_since_epoch);
            if (tosAcceptanceIpAddress.has_value()) body += "&tos_acceptance[ip]=" + tosAcceptanceIpAddress.value();
            if (userAccountId.has_value()) body += "&metadata[user_account_id]=" + std::to_string(userAccountId.value());

            if (birthDate.has_value()) {
                body += "&individual[dob][day]=" + DateUtils::getDayFromDate(birthDate.value());
                body += "&individual[dob][month]=" + DateUtils::getMonthFromDate(birthDate.value());
                body += "&individual[dob][year]=" + DateUtils::getYearFromDate(birthDate.value());
            }
            return body;
        }
    };
}


#endif //ECOMMERCE_BE_CREATEORUPDATESTRIPEACCOUNTDTO_H
