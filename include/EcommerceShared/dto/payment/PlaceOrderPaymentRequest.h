#ifndef ECOMMERCE_BE_PLACEORDERPAYMENTREQUEST_H
#define ECOMMERCE_BE_PLACEORDERPAYMENTREQUEST_H


#include <string>
#include <vector>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/dto/product/CartItemDTO.h"
#include "EcommerceShared/enums/delivery/DeliveryStatus.h"

namespace ecommerce::dto {

    struct PlaceOrderPaymentRequest {
        optional<long> orderId;
        optional<long> customerId;
        std::vector<CartItemDTO> items;

        JS_OBJ(orderId, customerId, items);

        void executeValidityCheck() const {
            if (!orderId.has_value()) throw std::invalid_argument("orderId must not be empty");
            if (!customerId.has_value()) throw std::invalid_argument("customerId must not be empty");
            if (orderId < 0) throw std::invalid_argument("orderId not valid");
            if (customerId < 0) throw std::invalid_argument("customerId not valid");
            if (items.empty()) throw std::invalid_argument("cart items vector must not be empty");
        }
    };
}


#endif //ECOMMERCE_BE_PLACEORDERPAYMENTREQUEST_H
