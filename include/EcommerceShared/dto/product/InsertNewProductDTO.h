//
// Created by federico on 24/02/24.
//

#ifndef ECOMMERCE_BE_INSERTNEWPRODUCTDTO_H
#define ECOMMERCE_BE_INSERTNEWPRODUCTDTO_H

#include <string>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/ControllerUtils.h"
#include "EcommerceShared/enums/product/ProductCategory.h"

namespace ecommerce::dto {
    struct InsertNewProductDTO {
        std::string upcCode;
        std::string name;
        std::string description;
        ProductCategory category = ProductCategory::NONE;
        float length{};
        float width{};
        float height{};
        float weight{};

        void executeValidityCheck() const {
            if (upcCode.empty()) throw std::invalid_argument("upcCode must not be empty");
            if (name.empty()) throw std::invalid_argument("name must not be empty");
            if (length < 0) throw std::invalid_argument("length must be non-negative");
            if (width < 0) throw std::invalid_argument("width must be non-negative");
            if (height < 0) throw std::invalid_argument("height must be non-negative");
            if (weight < 0) throw std::invalid_argument("weight must be non-negative");
        }

        JS_OBJ(upcCode, name, description, category, length, width, height, weight);
    };
}


// request body controller auto converters
namespace drogon {
    template<>
    inline ecommerce::dto::InsertNewProductDTO fromRequest(const HttpRequest &req) {
        return ControllerUtils::parseRequestBody<ecommerce::dto::InsertNewProductDTO>(req);
    }
}


#endif //ECOMMERCE_BE_INSERTNEWPRODUCTDTO_H
