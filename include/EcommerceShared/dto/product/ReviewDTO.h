#ifndef ECOMMERCE_BE_REVIEWDTO_H
#define ECOMMERCE_BE_REVIEWDTO_H

#include <string>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/ControllerUtils.h"
#include "EcommerceShared/enums/product/ProductCategory.h"
#include "EcommerceShared/dto/user/UserAccountDTO.h"

namespace ecommerce::dto {
    struct ReviewDTO {
        long id;
        long productId;
        long userAccountId;
        string title;
        string text;
        double rating;
        optional<std::string> createdAt;

        JS_OBJ(id, productId, userAccountId, title, text, rating, createdAt);
    };
}

// request body controller auto converters
namespace drogon {
    template<>
    inline ecommerce::dto::ReviewDTO fromRequest(const HttpRequest &req) {
        return ControllerUtils::parseRequestBody<ecommerce::dto::ReviewDTO>(req);
    }
}

#endif //ECOMMERCE_BE_REVIEWDTO_H
