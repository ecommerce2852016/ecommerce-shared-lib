//
// Created by federico on 09/03/24.
//

#ifndef ECOMMERCE_BE_BUYITEMSINCARTREQUESTDTO_H
#define ECOMMERCE_BE_BUYITEMSINCARTREQUESTDTO_H

#include <string>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/ControllerUtils.h"

namespace ecommerce::dto {

    struct BuyItemsInCartRequestDTO {
        optional<long> orderId;
        optional<long> customerId;


        void executeValidityCheck() const {
            if (!orderId.has_value()) throw std::invalid_argument("orderId must not be empty");
            if (!customerId.has_value()) throw std::invalid_argument("customerId must not be empty");
            if (orderId.value() < 0) throw std::invalid_argument("orderId invalid");
            if (customerId.value() < 0) throw std::invalid_argument("customerId invalid");
        }

        JS_OBJ(orderId, customerId);
    };
}

#endif //ECOMMERCE_BE_BUYITEMSINCARTREQUESTDTO_H
