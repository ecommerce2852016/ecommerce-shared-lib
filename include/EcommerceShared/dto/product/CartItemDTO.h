#ifndef ECOMMERCE_BE_CARTITEMDTO_H
#define ECOMMERCE_BE_CARTITEMDTO_H

#include <string>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/ControllerUtils.h"

namespace ecommerce::dto {
    struct CartItemDTO {
        long id;
        long quantityAvailable;
        long vendorId;

        long quantityInsideCart;
        float pricePerItem;
        float priceTotal;

        long supplyId;
        long productId;
        string productName;
        float productWeight;
        long fromAddressId;


        JS_OBJ(id, vendorId, quantityAvailable, quantityInsideCart, pricePerItem, priceTotal, supplyId, productId, productName, productWeight, fromAddressId);
    };
}


// request body controller auto converters
namespace drogon {
    template<>
    inline ecommerce::dto::CartItemDTO fromRequest(const HttpRequest &req) {
        return ControllerUtils::parseRequestBody<ecommerce::dto::CartItemDTO>(req);
    }
}

#endif //ECOMMERCE_BE_CARTITEMDTO_H
