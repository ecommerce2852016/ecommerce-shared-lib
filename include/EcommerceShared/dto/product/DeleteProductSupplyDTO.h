//
// Created by federico on 26/02/24.
//

#ifndef ECOMMERCE_BE_DELETEPRODUCTSUPPLYDTO_H
#define ECOMMERCE_BE_DELETEPRODUCTSUPPLYDTO_H

#include <string>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/ControllerUtils.h"

namespace ecommerce::dto {
    struct DeleteProductSupplyDTO {
        optional<long> productId;
        optional<long> vendorId;


        void executeValidityCheck() const {
            if (!productId.has_value()) throw std::invalid_argument("productId must not be empty");
            if (!vendorId.has_value()) throw std::invalid_argument("vendorId must not be empty");
            if (productId.value() < 0) throw std::invalid_argument("productId invalid");
            if (vendorId.value() < 0) throw std::invalid_argument("vendorId invalid");
        }

        JS_OBJ(productId, vendorId);
    };
}


// request body controller auto converters
namespace drogon {
    template<>
    inline ecommerce::dto::DeleteProductSupplyDTO fromRequest(const HttpRequest &req) {
        return ControllerUtils::parseRequestBody<ecommerce::dto::DeleteProductSupplyDTO>(req);
    }
}

#endif //ECOMMERCE_BE_DELETEPRODUCTSUPPLYDTO_H
