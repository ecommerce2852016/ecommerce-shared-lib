//
// Created by federico on 25/02/24.
//

#ifndef ECOMMERCE_BE_SAVEPRODUCTSUPPLYDTO_H
#define ECOMMERCE_BE_SAVEPRODUCTSUPPLYDTO_H


#include <string>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/ControllerUtils.h"

namespace ecommerce::dto {
    struct SaveProductSupplyDTO {
        optional<long> productId;
        optional<long> quantityVariation;
        optional<long> addressId;
        optional<float> price;


        void executeValidityCheck() const {
            if (!productId.has_value()) throw std::invalid_argument("productId must not be empty");
            if (!quantityVariation.has_value() && !price.has_value()) throw std::invalid_argument("quantityVariation and price can't be both empty");
            if (!addressId.has_value() || addressId.value() < 0) throw std::invalid_argument("addressId invalid or empty");
            if (productId.value() < 0) throw std::invalid_argument("productId invalid");
            if (price.value() < 0) throw std::invalid_argument("price invalid");
        }

        JS_OBJ(productId, quantityVariation, addressId, price);
    };
}


// request body controller auto converters
namespace drogon {
    template<>
    inline ecommerce::dto::SaveProductSupplyDTO fromRequest(const HttpRequest &req) {
        return ControllerUtils::parseRequestBody<ecommerce::dto::SaveProductSupplyDTO>(req);
    }
}

#endif //ECOMMERCE_BE_SAVEPRODUCTSUPPLYDTO_H
