//
// Created by federico on 24/02/24.
//

#ifndef ECOMMERCE_BE_PRODUCTDTO_H
#define ECOMMERCE_BE_PRODUCTDTO_H

#include <string>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/ControllerUtils.h"
#include "EcommerceShared/enums/product/ProductCategory.h"
#include "ProductSupplyDTO.h"

namespace ecommerce::dto {
    struct ProductDTO {
        long id;
        std::string upcCode;
        std::string name;
        std::string description;
        ProductCategory category;
        float length;
        float width;
        float height;

        vector<ProductSupplyDTO> supplies;
        long suppliesTotalQuantity;
        long suppliesQuantityAtPriceMin;

        float priceMin;
        float priceMax;

        JS_OBJ(id, upcCode, name, description, category, length, width, height, supplies, suppliesTotalQuantity, suppliesQuantityAtPriceMin, priceMin, priceMax);
    };
}


#endif //ECOMMERCE_BE_PRODUCTDTO_H
