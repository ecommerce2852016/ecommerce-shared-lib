#ifndef ECOMMERCE_BE_PRODUCTSUPPLYDTO_H
#define ECOMMERCE_BE_PRODUCTSUPPLYDTO_H


#include <string>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/ControllerUtils.h"
#include "EcommerceShared/enums/product/ProductCategory.h"
#include "EcommerceShared/dto/user/UserAccountDTO.h"

namespace ecommerce::dto {
    struct ProductSupplyDTO {
        long id;
        long vendorId;
        long quantity;
        float price;
        long addressId;


        JS_OBJ(id, vendorId, quantity, price, addressId);
    };
}


// request body controller auto converters
namespace drogon {
    template<>
    inline ecommerce::dto::ProductSupplyDTO fromRequest(const HttpRequest &req) {
        return ControllerUtils::parseRequestBody<ecommerce::dto::ProductSupplyDTO>(req);
    }
}

#endif //ECOMMERCE_BE_PRODUCTSUPPLYDTO_H
