#ifndef ECOMMERCE_BE_UPDATECARTITEMDTO_H
#define ECOMMERCE_BE_UPDATECARTITEMDTO_H


#include <string>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/ControllerUtils.h"

namespace ecommerce::dto {
    struct UpdateCartItemDTO {
        optional<long> customerId;
        optional<long> supplyId;
        optional<long> quantity;


        void executeValidityCheck() const {
            if (!customerId.has_value()) throw std::invalid_argument("customerId must not be empty");
            if (!supplyId.has_value()) throw std::invalid_argument("supplyId must not be empty");
            if (!quantity.has_value()) throw std::invalid_argument("quantity must not be empty");
            if (customerId.value() < 0) throw std::invalid_argument("customerId invalid");
            if (supplyId.value() < 0) throw std::invalid_argument("supplyId invalid");
            if (quantity.value() < 1) throw std::invalid_argument("quantity must be greater than 0");
        }

        JS_OBJ(customerId, supplyId, quantity);
    };
}


// request body controller auto converters
namespace drogon {
    template<>
    inline ecommerce::dto::UpdateCartItemDTO fromRequest(const HttpRequest &req) {
        return ControllerUtils::parseRequestBody<ecommerce::dto::UpdateCartItemDTO>(req);
    }
}

#endif //ECOMMERCE_BE_UPDATECARTITEMDTO_H
