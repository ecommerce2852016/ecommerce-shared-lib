
#ifndef ECOMMERCE_BE_STATUSRESPONSE_H
#define ECOMMERCE_BE_STATUSRESPONSE_H

#include "string"
#include "EcommerceShared/utils/json_struct.h"

struct StatusResponse {
    int code;
    std::string message;
    std::string environment;

    JS_OBJ(code, message, environment);

public:

    static StatusResponse of(int code, std::string message) {
        StatusResponse statusResponse;
        statusResponse.code = code;
        statusResponse.message = message;
        statusResponse.environment = drogon::app().getCustomConfig()["environment"].asString();
        return statusResponse;
    }

    Json::Value toJson() {
        Json::Value json;
        json["code"] = code;
        json["message"] = message;
        json["environment"] = environment;

        return json;
    }
};

#endif //ECOMMERCE_BE_STATUSRESPONSE_H
