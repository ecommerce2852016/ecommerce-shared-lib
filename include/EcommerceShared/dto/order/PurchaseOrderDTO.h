#ifndef ECOMMERCE_BE_PURCHASEORDERDTO_H
#define ECOMMERCE_BE_PURCHASEORDERDTO_H

#include <string>
#include <vector>
#include "EcommerceShared/utils/json_struct.h"
#include "OrderItemDTO.h"
#include "EcommerceShared/dto/delivery/ShipmentDTO.h"
#include "EcommerceShared/enums/delivery/DeliveryStatus.h"

namespace ecommerce::dto {

    struct PurchaseOrderDTO {
        long id = -1;
        long customerId;
        std::string createdAt;
        long shippingAddressId = -1;
        long billingAddressId = -1;

        std::vector<OrderItemDTO> items;
        float orderTotalPrice = 0;

        std::vector<ShipmentDTO> shipments;

        void executeValidityCheck() const {
            if (id < 0) throw std::invalid_argument("Invalid id");
            if (customerId < 0) throw std::invalid_argument("Invalid customerId");
            if (items.empty()) throw std::invalid_argument("Invalid items");
            for (auto &item: items) {
                item.executeValidityCheck();
            }
        }

        JS_OBJ(id, customerId, createdAt, shippingAddressId, billingAddressId, items, orderTotalPrice, shipments);
    };
}

#endif //ECOMMERCE_BE_PURCHASEORDERDTO_H
