#ifndef ECOMMERCE_BE_CREATENEWPURCHASEORDERDTO_H
#define ECOMMERCE_BE_CREATENEWPURCHASEORDERDTO_H

#include <string>
#include <vector>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/enums/delivery/DeliveryType.h"
#include "EcommerceShared/dto/delivery/ShipmentOptionDTO.h"
#include "EcommerceShared/enums/payment/PaymentMethod.h"
#include "EcommerceShared/utils/ControllerUtils.h"

namespace ecommerce::dto {

    struct CreateNewPurchaseOrderDTO {
        std::optional<long> customerId;
        std::optional<PaymentMethod> paymentMethod;
        std::vector<ShipmentOptionDTO> shipmentOptions;

        JS_OBJ(customerId, paymentMethod, shipmentOptions);

        void executeValidityCheck() {
            if (!customerId.has_value()) throw std::invalid_argument("customerId must not be empty");
            if (customerId.value() < 0) throw std::invalid_argument("customerId invalid");
            if (!paymentMethod.has_value()) throw std::invalid_argument("paymentMethod must not be empty");
        }
    };
}


// request body controller auto converters
namespace drogon {
    template<>
    inline ecommerce::dto::CreateNewPurchaseOrderDTO fromRequest(const HttpRequest &req) {
        return ControllerUtils::parseRequestBody<ecommerce::dto::CreateNewPurchaseOrderDTO>(req);
    }
}


#endif //ECOMMERCE_BE_CREATENEWPURCHASEORDERDTO_H
