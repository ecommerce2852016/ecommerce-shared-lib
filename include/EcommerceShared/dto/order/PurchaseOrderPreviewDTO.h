#ifndef ECOMMERCE_BE_PURCHASEORDERPREVIEWDTO_H
#define ECOMMERCE_BE_PURCHASEORDERPREVIEWDTO_H


#include <string>
#include <vector>
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/dto/product/CartItemDTO.h"
#include "EcommerceShared/dto/delivery/EPShippingData.h"
#include "EcommerceShared/dto/delivery/ShipmentPreviewDTO.h"
#include "EcommerceShared/enums/delivery/DeliveryStatus.h"
#include "EcommerceShared/dto/delivery/AddressDTO.h"

namespace ecommerce::dto {
    struct PurchaseOrderPreviewDTO {
        long customerId;
        optional<AddressDTO> shippingAddress;
        optional<AddressDTO> billingAddress;
        std::vector<ShipmentPreviewDTO> shipments;

        float itemsTotalPrice = 0;

        JS_OBJ(customerId, shippingAddress, billingAddress, shipments, itemsTotalPrice);
    };
}


#endif //ECOMMERCE_BE_PURCHASEORDERPREVIEWDTO_H
