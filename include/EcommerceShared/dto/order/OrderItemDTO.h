//
// Created by federico on 27/02/24.
//

#ifndef ECOMMERCE_BE_ORDERITEMDTO_H
#define ECOMMERCE_BE_ORDERITEMDTO_H

#include <string>
#include <vector>
#include "EcommerceShared/utils/json_struct.h"


namespace ecommerce::dto {

    struct OrderItemDTO {
        std::optional<long> id;

        long orderId;
        long productId;
        std::string productName;
        int quantity;
        double pricePerItem;
        double priceTotal;

        long vendorId;

        void executeValidityCheck() const {
            if (orderId <= 0) throw std::invalid_argument("orderId is required");
            if (productId <= 0) throw std::invalid_argument("productId is required");
            if (quantity <= 0) throw std::invalid_argument("quantity is required");
            if (pricePerItem <= 0) throw std::invalid_argument("pricePerItem is required");
            if (priceTotal <= 0) throw std::invalid_argument("priceTotal is required");
            if (vendorId <= 0) throw std::invalid_argument("vendorId is required");
        }

        JS_OBJ(id, productId, productName, quantity, pricePerItem, priceTotal, vendorId);
    };
}

#endif //ECOMMERCE_BE_ORDERITEMDTO_H
