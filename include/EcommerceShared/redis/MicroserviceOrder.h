//
// Created by federico on 23/04/24.
//

#ifndef ECOMMERCE_BE_MICROSERVICEORDER_H
#define ECOMMERCE_BE_MICROSERVICEORDER_H


#include <string>
#include "RedisStreamService.h"
#include "EcommerceShared/utils/StringUtils.h"
#include "EcommerceShared/dto/StatusResponse.h"

/**
 * Interface to the delivery microservice for cross-microservice calls
 */
class MicroServiceOrder {
public:

    static StatusResponse status(MicroserviceType sender) {
        auto message = RedisStreamService::execStreamRequestAndWaitForResponse<Json::Value>(ecommerce_stream, sender, orders_ms, "status", "", Json::Value(), "");

        if (message.getHttpStatusCode() != HttpStatusCode::k200OK) {
            auto text = message.asHttpResponse()->getJsonObject()->get("message", "Error in calling [order_ms] status from " + MicroserviceTypeConverter::toString(sender)).asString();
            return StatusResponse::of(message.getHttpStatusCode(), text);
        }

        auto status = message.getJsonAsStruct<StatusResponse>();
        return status;
    }

};

#endif //ECOMMERCE_BE_MICROSERVICEORDER_H
