//
// Created by federico on 13/02/24.
//

#ifndef ECOMMERCE_BE_RESPDESERIALIZER_H
#define ECOMMERCE_BE_RESPDESERIALIZER_H


#include <iostream>
#include <string>
#include <vector>
#include "StreamElement.h"


class RESPDeserializer {
public:
    /**
     * Deserializza un singolo elemento della stream da un XREAD con COUNT 1
     */
    static StreamElement deserializeOneElementXREAD(const drogon::nosql::RedisResult &result) {
//        LOG_DEBUG << "Message received: " << result.getStringForDisplaying();;
        if (result.isNil()) {
            return StreamElement();
        }

        auto respString = result.getStringForDisplaying();
        std::istringstream iss(respString);
        return parseSingleStreamElement(iss);
    }

private:
    static StreamElement parseSingleStreamElement(std::istringstream &iss) {
        std::string line;
        std::getline(iss, line);

        if (line.empty() || line[0] != '1' || line.find('"') == std::string::npos) {
            throw std::invalid_argument("Formato RESP non valido");
        }
        StreamElement streamElement;
        streamElement.streamName = StreamNameConverter::streamNameFromString(parseRESPString(line));

        std::vector<std::string> elements;
        std::string field;
        int i = 0;

        while (std::getline(iss, line)) {
            std::string parsedLine = parseRESPString(line);
            if (i == 0) {
                i++;
                streamElement.id = parsedLine;
                continue;
            }


            if (i % 2 == 1) {
                i++;
                field = parsedLine;
                continue;
            }

            if (line.empty()) {
                break;
            }

            streamElement.elements.push_back(FieldValue{field, parsedLine});
            i++;
        }

        return streamElement;
    }

    static std::string parseRESPString(const std::string &line) {
        size_t start = line.find_last_of(')') + 2;
        auto newString = line.substr(start);
        newString = newString.substr(1, newString.size() - 2);
        return newString;
    }
};


#endif //ECOMMERCE_BE_RESPDESERIALIZER_H