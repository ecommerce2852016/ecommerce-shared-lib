#ifndef ECOMMERCE_BE_STREAMELEMENT_H
#define ECOMMERCE_BE_STREAMELEMENT_H

#include <string>
#include "EcommerceShared/enums/StreamName.h"
#include "EcommerceShared/utils/json_struct.h"
#include "EcommerceShared/utils/StringUtils.h"
#include "EcommerceShared/utils/ControllerUtils.h"
#include "EcommerceShared/exceptions/http_exception.h"
#include "EcommerceShared/dto/user/UserAccountDTO.h"
#include "EcommerceShared/enums/MicroserviceType.h"
#include "EcommerceShared/utils/JWTUtils.h"


// Struttura per rappresentare un campo-valore
struct FieldValue {
    std::string key;
    std::string value;
};

// Struttura per rappresentare un elemento della stream
class StreamElement {
public:
    StreamName streamName;
    std::string id;
    std::vector<FieldValue> elements;

    StreamElement() {};

    void print() {
        std::cout << "StreamElement{ streamName=" << StreamNameConverter::toString(streamName) << " id=" << id << ", elements=[";
        for (const auto &element: elements) {
            std::cout << "\"" << element.key << "\": ";
            std::cout << "\"" << element.value << "\", ";
        }
        std::cout << "]}" << std::endl;
    }

    std::string getValue(const std::string &key, std::string defaultValue = "") const {
        for (const auto &element: elements) {
            if (element.key == key) {
                return element.value;
            }
        }
        return defaultValue;
    }

    Json::Value getJsonValue(const std::string &key = "json") const {
        auto jsonString = getValue(key);
        // convert to Json::Value
        Json::CharReaderBuilder rbuilder;
        std::unique_ptr<Json::CharReader> reader(rbuilder.newCharReader());
        Json::Value json;
        std::string errors;
        bool parsingSuccessful = reader->parse(jsonString.c_str(), jsonString.c_str() + jsonString.size(), &json, &errors);
        if (!parsingSuccessful) {
            LOG_ERROR << "Failed to parse JSON: " << errors;
        }
        return json;
    }

    template<typename T>
    T getJsonAsStruct(const std::string &key = "json") const {
        auto jsonString = getValue(key);
        return StringUtils::parseJsonString<T>(jsonString);
    }

    drogon::HttpStatusCode getHttpStatusCode(const std::string &key = "status_code") const {
        auto value = getValue(key);
        return static_cast<drogon::HttpStatusCode>(std::stoi(value));
    }

    std::shared_ptr<drogon::HttpResponse> asHttpResponse() const {
        auto jsonResponse = getJsonValue("json");
        auto httpResponse = drogon::HttpResponse::newHttpJsonResponse(jsonResponse);
        auto statusCode = getHttpStatusCode("status_code");
        httpResponse->setStatusCode(statusCode);
        return httpResponse;
    }

    optional<map<string, string>> getQueryParams(const std::string &key = "query_param") const {
        auto queryParam = getValue(key);
        if (!queryParam.empty()) {
            return ControllerUtils::queryParamFromString(queryParam);
        }
        return nullopt;
    }

    optional<string> getJwtToken() const {
        auto jwt_token = getValue("jwt_token", "");
        return jwt_token.empty() ? nullopt : make_optional(jwt_token);
    }

    optional<ecommerce::dto::UserAccountDTO> getUserFromToken() const {
        auto jwtToken = getJwtToken();
        if (jwtToken.has_value()) {
            auto user = JWTUtils::getUserAccountFromJWT(jwtToken.value());
            return user;
        }
        return nullopt;
    }

    optional<MicroserviceType> getSender() const {
        auto sender = getValue("sender", "");
        if (!sender.empty()) {
            return MicroserviceTypeConverter::msTypeFromString(sender);
        }
        return std::nullopt;
    }

    bool isNil() const {
        return id.empty();
    }
};

#endif //ECOMMERCE_BE_STREAMELEMENT_H
