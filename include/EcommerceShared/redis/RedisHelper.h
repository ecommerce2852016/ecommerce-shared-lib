//
// Created by federico on 14/02/24.
//

#ifndef ECOMMERCE_BE_REDISHELPER_H
#define ECOMMERCE_BE_REDISHELPER_H

#include <string>
#include <json/value.h>
#include <json/writer.h>
#include "EcommerceShared/exceptions/http_exception.h"
#include "EcommerceShared/utils/ControllerUtils.h"
#include "EcommerceShared/enums/MicroserviceType.h"
#include "EcommerceShared/enums/StreamName.h"
#include "deserializer/RESPDeserializer.h"
#include <csignal>
#include <atomic>

using namespace drogon;
using namespace drogon::nosql;
using namespace std;
using namespace ecommerce;
using namespace std::chrono;

/**
 * Helper class to interact directly with redis stream
 * @author Federico Agarinis, 2024
 */
class RedisHelper {
public:

    // static const variable
    static const bool XREAD_DEBUG_ENABLED = false;
    static const long STREAM_MAX_LEN = 10000L;
    inline static const std::string UNBLOCKER_REDIS_CLIENT_NAME = "unblocker";

    inline static std::atomic<bool> SIG_TERM_RECEIVED = false;

//    static bool initialize() {
//        LOG_DEBUG << "Initializing RedisHelper";
//        signal(SIGTERM, RedisHelper::handleSigTerm);
//        return true;
//    }

    /**
  * Send a xadd message to the stream to make a microservice receive and process a request
  * @param function_name identifier of the function to be called
  * @return the id of the message added to the stream
  */
    static string xaddExecFunction(const StreamName streamName, const MicroserviceType &sender, const MicroserviceType &receiver, const string &function_name, const string &query_param, const Json::Value &json, const string &jwt_token) {
        auto redis = drogon::app().getRedisClient();
        std::string jsonRedis = toRedisJson(json);

        LOG_COMPACT_DEBUG << "sending request message " << MicroserviceTypeConverter::toString(sender) << " --(" << function_name << ")--> " << MicroserviceTypeConverter::toString(receiver) << " ||  message: " << StreamNameConverter::toString(streamName) << " sender " + MicroserviceTypeConverter::toString(sender) + " receiver " + MicroserviceTypeConverter::toString(receiver) + " function_name " + function_name << (query_param.empty() ? "" : " query_param " + query_param) << " json " << jsonRedis << (jwt_token.empty() ? "" : " jwt_token " + jwt_token);
        auto xaddId = redis->execCommandSync([](const RedisResult &result) {
            return result.asString();
        }, "XADD " + StreamNameConverter::toString(streamName) + " * sender " + MicroserviceTypeConverter::toString(sender) + " receiver " + MicroserviceTypeConverter::toString(receiver) + " function_name " + function_name + " query_param %b json %b " + (jwt_token.empty() ? "" : " jwt_token " + jwt_token), query_param.data(), query_param.size(), jsonRedis.data(), jsonRedis.size());
        xtrimSize(streamName);

        return xaddId;
    }

    /**
    * Send a xadd message to the stream to make a microservice receive and process a request
    * @param function_name identifier of the function to be called
    * @return the id of the message added to the stream
    */
    template<typename T>
    static string xaddExecFunction(const StreamName streamName, const MicroserviceType &sender, const MicroserviceType &receiver, const string &function_name, const string &query_param, const T &structJsonSerializable, const string &jwt_token) {
        auto redis = drogon::app().getRedisClient();
        auto jsonString = JS::serializeStruct(structJsonSerializable, JS::SerializerOptions(JS::SerializerOptions::Compact));

        LOG_DEBUG << "REQUEST FOR FUNCTION: " << function_name << " - SENDING message: " << StreamNameConverter::toString(streamName) << " sender " + MicroserviceTypeConverter::toString(sender) + " receiver " + MicroserviceTypeConverter::toString(receiver) + " function_name " + function_name << (query_param.empty() ? "" : " query_param " + query_param) + " json " << jsonString << (jwt_token.empty() ? "" : " jwt_token " + jwt_token);
        auto xaddId = redis->execCommandSync([](const RedisResult &result) {
            return result.asString();
        }, "XADD " + StreamNameConverter::toString(streamName) + " * sender " + MicroserviceTypeConverter::toString(sender) + " receiver " + MicroserviceTypeConverter::toString(receiver) + " function_name " + function_name + " query_param %b json %b " + (jwt_token.empty() ? "" : " jwt_token " + jwt_token), query_param.data(), query_param.size(), jsonString.data(), jsonString.size());
        xtrimSize(streamName);

        return xaddId;
    }


    /**
     * Read messages starting by messages with ids greater than id_start_reading from the stream one-by-one usually from a call to xaddExecFunction, skipping messages until a condition is met
     * if no message arrives the thread will be blocked until a message arrives
     * @param id_start_reading \n $=start reading from the moment XREAD is called, \n\n  0=start reading from the beginning of the stream, \n\n  id=start reading from the message after the one with the given id
     * @return the first message that met the condition
     * @see RedisHelper::xAddExecFunction
     */
    static StreamElement xreadUntil(const StreamName streamName, const std::function<bool(const StreamElement &)> &untilCondition, const string &id_start_reading = "$") {
        auto redis = app().getRedisClient();

        string id = id_start_reading; // start reading from the first XREAD
        StreamElement streamElementRead;

        do {
            if (SIG_TERM_RECEIVED.load()) exit(0);
            LOG_DEBUG_IF(XREAD_DEBUG_ENABLED) << "Executing " << "XREAD BLOCK 0 COUNT 1 STREAMS " + StreamNameConverter::toString(streamName) + " " + id;
            streamElementRead = redis->execCommandSync(RESPDeserializer::deserializeOneElementXREAD, "XREAD BLOCK 0 COUNT 1 STREAMS " + StreamNameConverter::toString(streamName) + " " + id);

            if (streamElementRead.isNil()) {
                LOG_ERROR << "UserStreamConsumer::xreadUntil - streamElementRead is nil";
                continue; // skip nil messages
            }
            id = streamElementRead.id; // change read id for sequental XREAD (message ids are incremental);

        } while (!untilCondition(streamElementRead));

        return streamElementRead;
    }

    /**
     * Read messages starting by messages with ids greater than id_start_reading from the stream one-by-one usually from a call to xaddExecFunction, skipping messages until a condition is met
     * if no message arrives the thread will be blocked until a message arrives
     * @param id_start_reading \n $=start reading from the moment XREAD is called, \n\n  0=start reading from the beginning of the stream, \n\n  id=start reading from the message after the one with the given id
     * @param block_ms \n 0=blocked until condition is met \n\n  >0=blocked until condition is met or block_ms milliseconds has passed
     * @return the first message that met the condition
     * @see RedisHelper::xAddExecFunction
     */
    static optional <StreamElement> xreadUntilOrTimeout(const StreamName streamName, const std::function<bool(const StreamElement &)> &untilCondition, long block_ms, const string &id_start_reading = "$") {
        if (block_ms <= 0) {
            return xreadUntil(streamName, untilCondition, id_start_reading);
        }

        auto redis = app().getRedisClient();

        string id = id_start_reading; // start reading from the first XREAD
        StreamElement streamElementRead;

        auto begin = steady_clock::now(); // start counting ms
        const long TOTAL_MS = block_ms;
        do {
            if (SIG_TERM_RECEIVED.load()) exit(0);
            LOG_DEBUG_IF(XREAD_DEBUG_ENABLED) << "Executing " << "XREAD BLOCK " + to_string(block_ms) + " COUNT 1 STREAMS " + StreamNameConverter::toString(streamName) + " " + id;
            streamElementRead = redis->execCommandSync(RESPDeserializer::deserializeOneElementXREAD, "XREAD BLOCK " + to_string(block_ms) + " COUNT 1 STREAMS " + StreamNameConverter::toString(streamName) + " " + id);
            block_ms = TOTAL_MS - duration_cast<milliseconds>(steady_clock::now() - begin).count();
            if (block_ms <= 0) {
                LOG_DEBUG << "Timeout expired";
                return nullopt;
            }

            if (streamElementRead.isNil()) {
                LOG_ERROR << "UserStreamConsumer::xreadUntil - streamElementRead is nil";
                continue; // skip nil messages
            }
            id = streamElementRead.id; // change read id for sequental XREAD (message ids are incremental);

        } while (!untilCondition(streamElementRead));
        return streamElementRead;
    }


    static StreamElement xreadUntilReceiverEqual(const StreamName streamName, const MicroserviceType &receiver, const string &id_start_reading = "$") {
        return xreadUntil(streamName, [receiver](const StreamElement &streamElement) {
            return streamElement.getValue("receiver") == MicroserviceTypeConverter::toString(receiver);
        }, id_start_reading);
    }

    static StreamElement xreadUntilReceiverIdEqual(const StreamName streamName, const string &receiver_id, const string &id_start_reading = "$") {
        return xreadUntil(streamName, [receiver_id](const StreamElement &streamElement) {
            return streamElement.getValue("receiver_id") == receiver_id;
        }, id_start_reading);
    }

    static optional <StreamElement> xreadUntilReceiverEqualOrTimeout(const StreamName streamName, const MicroserviceType &receiver, long block_ms, const string &id_start_reading = "$") {
        return xreadUntilOrTimeout(streamName, [receiver](const StreamElement &streamElement) {
            return streamElement.getValue("receiver") == MicroserviceTypeConverter::toString(receiver);
        }, block_ms, id_start_reading);
    }

    static optional <StreamElement> xreadUntilReceiverIdEqualOrTimeout(const StreamName streamName, const string &receiver_id, long block_ms, const string &id_start_reading = "$") {
        return xreadUntilOrTimeout(streamName, [receiver_id](const StreamElement &streamElement) {
            return streamElement.getValue("receiver_id") == receiver_id;
        }, block_ms, id_start_reading);
    }


    /**
 * Convert a Json::Value to a string that can be insert into redis command
 * @example redisJson=toRedisJson(inputJson) \n "XADD ecommerce_stream * inputJson %b", redisJson.data(), redisJson.size());
 * @param inputJson
 * @return redisJson
 */
    static std::string toRedisJson(const Json::Value &inputJson) {
        Json::StreamWriterBuilder wbuilder;
        wbuilder["indentation"] = ""; // If you want whitespace-less output
        std::string jsonString = Json::writeString(wbuilder, inputJson);
        return jsonString;
    }


    /**
     * removes the oldest messages from the stream until the size is less than the given size
     * this does not set a hard limit, if many entries are added at once the stream may exceed the size
     */
    static void xtrimSize(const StreamName streamName, const long size = STREAM_MAX_LEN) {
        auto redis = drogon::app().getRedisClient();
        redis->execCommandSync([](const RedisResult &result) {
            return result.asString();
        }, "XTRIM " + StreamNameConverter::toString(streamName) + " MAXLEN " + to_string(size));
    }

    static void setupKillClientOnSigTerm(std::string clientId) {
        // subscribe to sigterm
        drogon::app().setTermSignalHandler([clientId]() {
            SIG_TERM_RECEIVED.store(true);
            LOG_DEBUG << "[RedisHelper.setupKillClientOnSigTerm] looking for redis client named \""<< UNBLOCKER_REDIS_CLIENT_NAME<<"\"";
            app().getRedisClient(UNBLOCKER_REDIS_CLIENT_NAME)->execCommandAsync([](const RedisResult &r) {
                LOG_INFO << "Sucessfully unblocked client id: " << r.asString();
            }, [clientId](const RedisException &r) {
                LOG_ERROR << "Error shutting down: " << r.what();
            }, "CLIENT UNBLOCK " + clientId);
        });
    }

private:
};

#endif //ECOMMERCE_BE_REDISHELPER_H
