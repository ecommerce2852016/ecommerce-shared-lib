#ifndef ECOMMERCE_BE_MICROSERVICEDELIVERY_H
#define ECOMMERCE_BE_MICROSERVICEDELIVERY_H

#include <string>
#include "RedisStreamService.h"
#include "EcommerceShared/dto/payment/CreateNewStripeCustomerDTO.h"
#include "EcommerceShared/dto/payment/CreateOrUpdateStripeAccountDTO.h"
#include "EcommerceShared/dto/payment/PlaceOrderPaymentRequest.h"
#include "EcommerceShared/dto/delivery/ShipmentPreviewRequestDTO.h"
#include "EcommerceShared/dto/delivery/ShipmentDTO.h"
#include "EcommerceShared/dto/delivery/CreateShipmentRequestDTO.h"
#include "EcommerceShared/dto/delivery/ShipmentPreviewDTO.h"
#include "EcommerceShared/utils/StringUtils.h"
#include "EcommerceShared/dto/delivery/AddressDTO.h"
#include "EcommerceShared/dto/StatusResponse.h"

using namespace ecommerce::dto;


/**
 * Interface to the delivery microservice for cross-microservice calls
 */
class MicroserviceDelivery {
public:

    static StatusResponse status(MicroserviceType sender) {
        auto message = RedisStreamService::execStreamRequestAndWaitForResponse<Json::Value>(ecommerce_stream, sender, delivery_ms, "status", "", Json::Value(), "");

        if (message.getHttpStatusCode() != HttpStatusCode::k200OK) {
            auto text = message.asHttpResponse()->getJsonObject()->get("message", "Error in calling [delivery_ms] status from " + MicroserviceTypeConverter::toString(sender)).asString();
            return StatusResponse::of(message.getHttpStatusCode(), text);
        }

        auto status = message.getJsonAsStruct<StatusResponse>();
        return status;
    }

    static vector<AddressDTO> getUserAddresses(MicroserviceType sender, const long user_account_id) {
        Json::Value json;
        json["user_account_id"] = to_string(user_account_id);

        auto message = RedisStreamService::execStreamRequestAndWaitForResponse<Json::Value>(ecommerce_stream, sender, delivery_ms, "getUserAddresses", "", json, "");

        if (message.getHttpStatusCode() != HttpStatusCode::k200OK) {
            auto text = message.asHttpResponse()->getJsonObject()->get("message", "Error in calling [delivery_ms] getCustomerCartItems from " + MicroserviceTypeConverter::toString(sender)).asString();
            throw ecommerce::exceptions::http_exception(text, message.getHttpStatusCode());
        }

        auto addressess = message.getJsonAsStruct<vector<AddressDTO>>();
        return addressess;
    }

    static optional<AddressDTO> getUserShippingAddress(MicroserviceType sender, const long user_account_id) {
        auto addresses = getUserAddresses(sender, user_account_id);
        return addresses.empty() ? nullopt : make_optional<>(addresses.front());
    }

    static optional<AddressDTO> getUserBillingAddress(MicroserviceType sender, const long user_account_id) {
        auto addresses = getUserAddresses(sender, user_account_id);
        return addresses.empty() ? nullopt : make_optional<>(addresses.front());
    }

    static vector<ShipmentPreviewDTO> getShipmentPreview(MicroserviceType sender, const long toAddressId, vector<CartItemDTO> &cartItems) {
        ShipmentPreviewRequestDTO shipmentRequestDTO;
        shipmentRequestDTO.toAddressId = toAddressId;
        shipmentRequestDTO.items = cartItems;

        auto message = RedisStreamService::execStreamRequestAndWaitForResponse<ShipmentPreviewRequestDTO>(ecommerce_stream, sender, delivery_ms, "getShipmentPreview", "", shipmentRequestDTO, "");

        if (message.getHttpStatusCode() != HttpStatusCode::k200OK) {
            auto text = message.asHttpResponse()->getJsonObject()->get("message", "Error in calling [delivery_ms] getShipmentPreview from " + MicroserviceTypeConverter::toString(sender)).asString();
            throw ecommerce::exceptions::http_exception(text, message.getHttpStatusCode());
        }

        auto shipmentPreviews = message.getJsonAsStruct<vector<ShipmentPreviewDTO>>();
        return shipmentPreviews;
    }

    static ShipmentDTO createNewShipment(MicroserviceType sender, const CreateShipmentRequestDTO &shipmentRequestDTO) {
        auto message = RedisStreamService::execStreamRequestAndWaitForResponse<CreateShipmentRequestDTO>(ecommerce_stream, sender, delivery_ms, "createNewShipment", "", shipmentRequestDTO, "");

        if (message.getHttpStatusCode() != HttpStatusCode::k200OK) {
            auto text = message.asHttpResponse()->getJsonObject()->get("message", "Error in calling [delivery_ms] createNewShipment from " + MicroserviceTypeConverter::toString(sender)).asString();
            throw ecommerce::exceptions::http_exception(text, message.getHttpStatusCode());
        }

        auto shipment = message.getJsonAsStruct<ShipmentDTO>();
        return shipment;
    }

    static vector<ShipmentDTO> findAllShipmentsByOrderId(MicroserviceType sender, const long orderId) {
        Json::Value json;
        json["orderId"] = to_string(orderId);

        auto message = RedisStreamService::execStreamRequestAndWaitForResponse<Json::Value>(ecommerce_stream, sender, delivery_ms, "findAllShipmentsByOrderId", "", json, "");

        if (message.getHttpStatusCode() != HttpStatusCode::k200OK) {
            auto text = message.asHttpResponse()->getJsonObject()->get("message", "Error in calling [delivery_ms] findAllShipmentsByOrderId from " + MicroserviceTypeConverter::toString(sender)).asString();
            throw ecommerce::exceptions::http_exception(text, message.getHttpStatusCode());
        }

        auto shipments = message.getJsonAsStruct<vector<ShipmentDTO>>();
        return shipments;
    }

};


#endif //ECOMMERCE_BE_MICROSERVICEDELIVERY_H
