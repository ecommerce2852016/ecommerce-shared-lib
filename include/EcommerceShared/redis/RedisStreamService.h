//
// Created by federico on 15/02/24.
//

#ifndef ECOMMERCE_BE_REDISSTREAMSERVICE_H
#define ECOMMERCE_BE_REDISSTREAMSERVICE_H

#include <string>
#include <json/value.h>
#include <json/writer.h>
#include "EcommerceShared/exceptions/http_exception.h"
#include "EcommerceShared/utils/ControllerUtils.h"
#include "EcommerceShared/enums/MicroserviceType.h"
#include "EcommerceShared/enums/StreamName.h"
#include "deserializer/RESPDeserializer.h"
#include "RedisHelper.h"

using namespace drogon;
using namespace drogon::nosql;
using namespace std;
using namespace ecommerce;
using namespace std::chrono;

/**
 * Helper class to interact directly with redis stream
 * @author Federico Agarinis, 2024
 */
class RedisStreamService {
public:

    static const bool DEBUG_ENABLED = false;

    /**
     * Send a xadd message to the stream to make a microservice process a request using function_name as identifier and wait for a response until timeout
     * @param reqBody must be serializable to json @see JS::serializeStruct
     * @return
     */
    using None = std::monostate;

    /**
     * Send a xadd message to the stream to make a microservice process a request using function_name as identifier and wait for a response until timeout
     * @param reqBody must be serializable to json @see JS::serializeStruct
     * @return
     */
    template<typename T = None>
    static StreamElement execStreamRequestAndWaitForResponse(const StreamName streamName,
                                                             const MicroserviceType &sender,
                                                             const MicroserviceType &receiver,
                                                             const string &function_name,
                                                             const optional<string> &query_param = nullopt,
                                                             const optional<T> &reqBody = nullopt,
                                                             const string &jwtToken = "",
                                                             const long timeout_ms = 20000) {
        string message_id;
        if constexpr (std::is_same_v<T, None>) {
            message_id = RedisHelper::xaddExecFunction(streamName, sender, receiver, function_name, query_param.value_or(""), {}, jwtToken);
        } else if constexpr (std::is_same_v<T, Json::Value>) {
            message_id = RedisHelper::xaddExecFunction(streamName, sender, receiver, function_name, query_param.value_or(""), reqBody.value(), jwtToken);
        } else {
            message_id = RedisHelper::xaddExecFunction<T>(streamName, sender, receiver, function_name, query_param.value_or(""), reqBody.value(), jwtToken);
        }
        LOG_DEBUG << "REQUEST FOR FUNCTION: " << function_name << " - WAITING until message with receiver_id " << message_id << " FROM " << MicroserviceTypeConverter::toString(receiver) << " is received";
        optional<StreamElement> streamElement = RedisHelper::xreadUntilReceiverIdEqualOrTimeout(streamName, message_id, timeout_ms, message_id);
        if (!streamElement.has_value()) {
            throw ecommerce::exceptions::http_exception("Request Timeout of api_gateway waiting for stream message of " + MicroserviceTypeConverter::toString(receiver), k408RequestTimeout);
        } else {
            return streamElement.value();
        }
    }


    // start listening on redis for message sent to a specific receiver and use a map of function_name to function to process the message
    static void listenAndProcessStreamRequests(StreamName streamName, MicroserviceType receiver, std::map<std::string, std::function<void(const StreamElement &)>> messageHandlerMap) {
        string id_start_reading = "$";
        do {
            LOG_DEBUG_IF(DEBUG_ENABLED) << "waiting for next message...";
            auto message = RedisHelper::xreadUntilReceiverEqual(streamName, receiver, id_start_reading);
            id_start_reading = message.id; // prepare the id for the next read
            try {
                auto function_name = message.getValue("function_name");
                if (function_name.empty()) {
                    LOG_DEBUG_IF(DEBUG_ENABLED) << "ignored message with empty function_name";
                    continue; // ignore request without function_name. for example the exceptions received from another microservices consumer
                }
                LOG_COMPACT_DEBUG << "Received message with function_name: " + function_name;
                if (messageHandlerMap.find(function_name) == messageHandlerMap.end()) {
                    throw ecommerce::exceptions::http_exception("Bad message in redis stream: function_name " + function_name + " not handled by " + MicroserviceTypeConverter::toString(receiver) + " consumer", k404NotFound);
                }
                auto messageHandler = messageHandlerMap.at(function_name);
                messageHandler(message);
            } catch (const std::exception &e) {
                LOG_ERROR << "RedisStreamService::listenAndProcessStreamRequests - error handling message: " << e.what();
                xaddStreamExceptionToSender(message.streamName, message, e);
            }

        } while (true);
    }


    /**
 * Send a XADD message to the stream to answer the sender
 * @param receiver the microservices that is the receiver of this message
 * @param receiver_id the id of the message that this message is answering
 * @return the id of the message added to the stream XADD
 */
    static string xaddStreamResponseMessage(const StreamName streamName, const MicroserviceType &receiver, const string &receiver_id, HttpStatusCode httpStatusCode, const Json::Value &jsonResponse) {
        Json::StreamWriterBuilder wbuilder;
        wbuilder["indentation"] = ""; // If you want whitespace-less output
        auto responseJsonString = Json::writeString(wbuilder, jsonResponse);
        return xaddStreamResponseMessage(streamName, receiver, receiver_id, httpStatusCode, responseJsonString);
    }


    /**
     *  Send a XADD message to the stream with a specific response to specific receiver id
     * @param response struct that must have JS_OBJ macro defined to be serialized to json
     * @return the id of the message added to the stream with XADD
     */
    template<typename T>
    static string xaddStreamResponseMessage(const StreamName streamName, const MicroserviceType &receiver, const string &receiver_id, HttpStatusCode httpStatusCode, const T response) {
        auto responseJsonString = JS::serializeStruct(response, JS::SerializerOptions(JS::SerializerOptions::Compact));
        return xaddStreamResponseMessage(streamName, receiver, receiver_id, httpStatusCode, responseJsonString);
    }


    /**
    * Send a XADD message to the stream with a specific response to specific receiver id
    * @param receiver the microservices that is the receiver of this message
    * @param receiver_id the id of the message that this message is answering
    * @return the id of the message added to the stream with XADD
    */
    static string xaddStreamResponseMessage(const StreamName streamName, const MicroserviceType &receiver, const string &receiver_id, HttpStatusCode httpStatusCode, const std::string &responseJsonString) {
        auto redis = drogon::app().getRedisClient();

        LOG_DEBUG << "sending response message to ----> " << MicroserviceTypeConverter::toString(receiver)  << " || message: " << StreamNameConverter::toString(streamName) << " receiver " + MicroserviceTypeConverter::toString(receiver) + " receiver_id " + receiver_id + " status_code " + std::to_string(httpStatusCode) + " json " << responseJsonString;
        auto xaddId = redis->execCommandSync([](const RedisResult &result) {
            return result.asString();
        }, "XADD " + StreamNameConverter::toString(streamName) + " * receiver " + MicroserviceTypeConverter::toString(receiver) + " receiver_id " + receiver_id + " status_code " + std::to_string(httpStatusCode) + " json %b", responseJsonString.data(), responseJsonString.size());

        return xaddId;
    }


    template<typename T>
    static void xaddStreamResponseToSender(const StreamName streamName, const StreamElement &messageOfSender, const T &response) {
        auto redis = drogon::app().getRedisClient();

        basic_string<char> sender_id = messageOfSender.id; // id of the sender waiting for the message
        auto senderString = messageOfSender.getValue("sender");
        MicroserviceType sender = MicroserviceTypeConverter::msTypeFromString(senderString);


        xaddStreamResponseMessage<T>(messageOfSender.streamName, sender, sender_id, k200OK, response);
    }


    static void xaddStreamExceptionToSender(const StreamName streamName, const StreamElement &messageOfSender, const std::exception &e) {
        auto redis = drogon::app().getRedisClient();

        auto sender_id = messageOfSender.id; // id of the sender waiting for the message
        auto senderString = messageOfSender.getValue("sender");
        MicroserviceType sender = MicroserviceTypeConverter::msTypeFromString(senderString);

        LOG_ERROR << "sending exception to sender ----> " << senderString << " message id " << sender_id;

        if (sender_id.empty()) {
            LOG_INFO << "UserStreamConsumer::xaddStreamExceptionToSender - messageOfSender id or sender is empty, could not send exception to sender";
            return;
        }

        Json::Value errorJson;
        HttpStatusCode httpStatusCode;
        // Verifica se l'eccezione è di tipo http_exception personalizzato
        if (const auto *httpException = dynamic_cast<const ecommerce::exceptions::http_exception *>(&e)) {
            httpStatusCode = httpException->getHttpStatusCode();
        } else {
            httpStatusCode = k500InternalServerError; // 500 generico
        }
        errorJson = ControllerUtils::simpleJsonResponse(e.what(), httpStatusCode);

        xaddStreamResponseMessage(streamName, sender, messageOfSender.id, httpStatusCode, errorJson);
    }

};


#endif //ECOMMERCE_BE_REDISSTREAMSERVICE_H
