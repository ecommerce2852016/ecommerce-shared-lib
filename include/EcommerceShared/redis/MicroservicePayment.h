#ifndef ECOMMERCE_BE_MICROSERVICEPAYMENT_H
#define ECOMMERCE_BE_MICROSERVICEPAYMENT_H

#include <string>
#include "RedisStreamService.h"
#include "EcommerceShared/dto/payment/CreateNewStripeCustomerDTO.h"
#include "EcommerceShared/dto/payment/CreateOrUpdateStripeAccountDTO.h"
#include "EcommerceShared/dto/payment/PlaceOrderPaymentRequest.h"
#include "EcommerceShared/utils/StringUtils.h"
#include "EcommerceShared/dto/StatusResponse.h"

using namespace ecommerce::dto;


/**
 * Interface to the payment microservice for cross-microservice calls
 */
class MicroservicePayment {
public:

    static StatusResponse status(MicroserviceType sender) {
        auto message = RedisStreamService::execStreamRequestAndWaitForResponse<Json::Value>(ecommerce_stream, sender, payments_ms, "status", "", Json::Value(), "");

        if (message.getHttpStatusCode() != HttpStatusCode::k200OK) {
            auto text = message.asHttpResponse()->getJsonObject()->get("message", "Error in calling [payments_ms] status from " + MicroserviceTypeConverter::toString(sender)).asString();
            return StatusResponse::of(message.getHttpStatusCode(), text);
        }

        auto status = message.getJsonAsStruct<StatusResponse>();
        return status;
    }

    static StreamElement createNewStripeCustomer(MicroserviceType sender, const CreateNewStripeCustomerDTO &createNewStripeCustomerDto) {
        auto message = RedisStreamService::execStreamRequestAndWaitForResponse<CreateNewStripeCustomerDTO>(ecommerce_stream, sender, payments_ms, "createNewStripeCustomer", "", createNewStripeCustomerDto, "");
        return message;
    }

    static StreamElement createOrUpdateStripeAccount(MicroserviceType sender, const CreateOrUpdateStripeAccountDTO &createOrUpdateStripeAccountDto) {
        auto message = RedisStreamService::execStreamRequestAndWaitForResponse<CreateOrUpdateStripeAccountDTO>(ecommerce_stream, sender, payments_ms, "createNewOrUpdateStripeAccount", "", createOrUpdateStripeAccountDto, "");
        return message;
    }

    static StreamElement execSendPaymentTransactionForOrder(MicroserviceType sender, const PlaceOrderPaymentRequest &placeOrderPaymentRequest) {
        auto message = RedisStreamService::execStreamRequestAndWaitForResponse<PlaceOrderPaymentRequest>(ecommerce_stream, sender, payments_ms, "sendPaymentTransactionForOrder", "", placeOrderPaymentRequest, "");
        return message;
    }

};

#endif //ECOMMERCE_BE_MICROSERVICEPAYMENT_H
