//
// Created by federico on 27/02/24.
//

#ifndef ECOMMERCE_BE_MICROSERVICEPRODUCT_H
#define ECOMMERCE_BE_MICROSERVICEPRODUCT_H


#include <string>
#include "EcommerceShared/dto/product/CartItemDTO.h"
#include "EcommerceShared/dto/product/BuyItemsInCartRequestDTO.h"
#include "EcommerceShared/dto/StatusResponse.h"
#include "RedisStreamService.h"

using namespace ecommerce::dto;

/**
 * Interface to the user microservice for cross-microservice calls
 */
class MicroserviceProduct {
public:

    static StatusResponse status(MicroserviceType sender) {
        auto message = RedisStreamService::execStreamRequestAndWaitForResponse<Json::Value>(ecommerce_stream, sender, product_ms, "status", "", Json::Value(), "");

        if (message.getHttpStatusCode() != HttpStatusCode::k200OK) {
            auto text = message.asHttpResponse()->getJsonObject()->get("message", "Error in calling [product_ms] status from " + MicroserviceTypeConverter::toString(sender)).asString();
            return StatusResponse::of(message.getHttpStatusCode(), text);
        }

        auto status = message.getJsonAsStruct<StatusResponse>();
        return status;
    }

    /**
     * @throws http_exception if the user is not found
     */
    static vector<CartItemDTO> getCustomerCartItems(MicroserviceType sender, long customerId) {
        Json::Value json;
        json["userId"] = to_string(customerId);
        auto message = RedisStreamService::execStreamRequestAndWaitForResponse<Json::Value>(ecommerce_stream, sender, product_ms, "findCartItemsOfCustomer", "", json, "");

        if (message.getHttpStatusCode() != HttpStatusCode::k200OK) {
            auto text = message.asHttpResponse()->getJsonObject()->get("message", "Error in calling [product_ms] getCustomerCartItems from " + MicroserviceTypeConverter::toString(sender)).asString();
            throw ecommerce::exceptions::http_exception(text, message.getHttpStatusCode());
        }

        auto cartItems = message.getJsonAsStruct<vector<CartItemDTO>>();
        return cartItems;
    }

    /**
 * @throws http_exception if payment error
 */
    static vector<CartItemDTO> buyItemsInCart(MicroserviceType sender, BuyItemsInCartRequestDTO buyItemsInCartRequestDto) {
        auto message = RedisStreamService::execStreamRequestAndWaitForResponse<BuyItemsInCartRequestDTO>(ecommerce_stream, sender, product_ms, "buyItemsInCart", "", buyItemsInCartRequestDto, "");

        if (message.getHttpStatusCode() != HttpStatusCode::k200OK) {
            auto text = message.asHttpResponse()->getJsonObject()->get("message", "Error in calling [product_ms] buyItemsInCart from " + MicroserviceTypeConverter::toString(sender)).asString();
            throw ecommerce::exceptions::http_exception(text, message.getHttpStatusCode());
        }

        auto cartItems = message.getJsonAsStruct<vector<CartItemDTO>>();
        return cartItems;
    }


};

#endif //ECOMMERCE_BE_MICROSERVICEPRODUCT_H
