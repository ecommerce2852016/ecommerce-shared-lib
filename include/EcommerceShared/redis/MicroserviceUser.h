#ifndef ECOMMERCE_BE_MICROSERVICEUSER_H
#define ECOMMERCE_BE_MICROSERVICEUSER_H

#include <string>
#include "EcommerceShared/dto/user/UserAccountDTO.h"
#include "EcommerceShared/dto/StatusResponse.h"
#include "RedisStreamService.h"

using namespace ecommerce::dto;

/**
 * Interface to the user microservice for cross-microservice calls
 */
class MicroserviceUser {
public:


    static StatusResponse status(MicroserviceType sender) {
        auto message = RedisStreamService::execStreamRequestAndWaitForResponse<Json::Value>(ecommerce_stream, sender, user_ms, "status", "", Json::Value(), "");

        if (message.getHttpStatusCode() != HttpStatusCode::k200OK) {
            auto text = message.asHttpResponse()->getJsonObject()->get("message", "Error in calling [user_ms] status from " + MicroserviceTypeConverter::toString(sender)).asString();
            return StatusResponse::of(message.getHttpStatusCode(), text);
        }

        auto status = message.getJsonAsStruct<StatusResponse>();
        return status;
    }

    /**
     * @throws http_exception if the user is not found
     */
    static UserAccountDTO findUserById(MicroserviceType sender, long userId) {
        Json::Value json;
        json["userId"] = to_string(userId);
        auto message = RedisStreamService::execStreamRequestAndWaitForResponse<Json::Value>(ecommerce_stream, sender, user_ms, "findUserById", nullopt, json);

        if (message.getHttpStatusCode() != HttpStatusCode::k200OK) {
            auto text = message.asHttpResponse()->getJsonObject()->get("message", "Error in calling [user_ms] findUserById from " + MicroserviceTypeConverter::toString(sender)).asString();
            throw ecommerce::exceptions::http_exception(text, message.getHttpStatusCode());
        }
        auto userAccountDto = message.getJsonAsStruct<UserAccountDTO>();
        return userAccountDto;
    }

};

#endif //ECOMMERCE_BE_MICROSERVICEUSER_H
