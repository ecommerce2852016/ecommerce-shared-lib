//
// Created by federico on 14/02/24.
//

#ifndef ECOMMERCE_BE_HTTP_EXCEPTION_H
#define ECOMMERCE_BE_HTTP_EXCEPTION_H


#include <stdexcept>
#include <string>
#include <drogon/HttpTypes.h>

namespace ecommerce {
    namespace exceptions {
        class http_exception : public std::exception {
        public:
            http_exception(const std::string &message, drogon::HttpStatusCode httpStatusCode) : m_message(message), m_httpStatusCode(httpStatusCode) {}

            const char *what() const noexcept override { return m_message.c_str(); }

            drogon::HttpStatusCode getHttpStatusCode() const noexcept { return m_httpStatusCode; }

        private:
            std::string m_message;
            drogon::HttpStatusCode m_httpStatusCode;
        };

    }

}

#endif //ECOMMERCE_BE_HTTP_EXCEPTION_H
