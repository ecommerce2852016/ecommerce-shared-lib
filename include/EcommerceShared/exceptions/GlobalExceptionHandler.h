//
// Created by federico on 15/02/24.
//

#ifndef ECOMMERCE_BE_GLOBALEXCEPTIONHANDLER_H
#define ECOMMERCE_BE_GLOBALEXCEPTIONHANDLER_H

#include "EcommerceShared/utils/ControllerUtils.h"
#include "http_exception.h"

namespace ecommerce::exceptions {

    class GlobalExceptionHandler {
    public:
        static void handle(const std::exception &e,
                           const drogon::HttpRequestPtr &req,
                           std::function<void(const drogon::HttpResponsePtr &)> &&cb) {
            LOG_ERROR << "[globalExceptionHandler] " << req->path()<< " Handle exception: " << e.what();
            // Verifica se l'eccezione è di tipo http_exception personalizzato
            if (const auto *httpException = dynamic_cast<const http_exception *>(&e)) {
                // Recupera il codice di stato HTTP dall'eccezione
                HttpStatusCode httpStatusCode = httpException->getHttpStatusCode();
                auto errorResponse = ControllerUtils::errorJsonResponse(e.what(), httpStatusCode);
                cb(errorResponse);
            } else {
                // Se l'eccezione non è di tipo http_exception personalizzato,
                // restituisci un codice di stato HTTP 500 Internal Server Error di default
                auto errorResponse = ControllerUtils::errorJsonResponse(e.what(), drogon::HttpStatusCode::k500InternalServerError);
                cb(errorResponse);
            }
        }
    };
}


#endif //ECOMMERCE_BE_GLOBALEXCEPTIONHANDLER_H
