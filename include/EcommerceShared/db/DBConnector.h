#ifndef ECOMMERCE_BE_DBCONNECTOR_H
#define ECOMMERCE_BE_DBCONNECTOR_H

#include <string>
#include <json/value.h>
#include <drogon/HttpAppFramework.h>
#include <drogon/HttpController.h>
#include <postgresql/libpq-fe.h>
#include <iostream>
#include <fstream>
#include <json/json.h>
#include "EcommerceShared/utils/EcommerceConfigLoader.h"


class DBConnector {
    // variabile di debug SHOW_SQL
    static const bool SHOW_SQL = false;

public:
    static void initDB() {
        PGconn *connection = connect2DB("postgres");

        if (PQstatus(connection) != CONNECTION_OK) {
            LOG_ERROR << "Connessione al database postgres fallita: " << PQerrorMessage(connection);
            PQfinish(connection);
            return;
        }

        // Query per verificare se il database esiste già
        const std::string dbName = EcommerceConfigLoader::getDbName(0);
        bool dbExists = databaseExists(connection, dbName.c_str());
        if (!dbExists) {
            createDatabase(connection, dbName.c_str());
            PQfinish(connection);

            // Ricrea la connessione al database appena creato
            connection = connect2DB(dbName.c_str());
            createTables(connection);
            PQfinish(connection);
        } else {
            PQfinish(connection);
            LOG_DEBUG << "Database " << dbName << " already exists skipping creation";
        }

        LOG_DEBUG << "initDB END";
    }

private:
    static PGconn *connect2DB(const std::string &dbName) {
        Json::Value config = EcommerceConfigLoader::getConfig();
        std::string user = config["db_clients"][0]["user"].asString();
        std::string passwd = config["db_clients"][0]["passwd"].asString();
        std::string host = config["db_clients"][0]["host"].asString();
        std::string port = config["db_clients"][0]["port"].asString();
        LOG_DEBUG << "initializing DB connection to " << std::string(dbName) << " with user " << user << " on host " << host << " and port " << port;

        std::string connInfo = "dbname=" + std::string(dbName) + " user=" + user + " password=" + passwd + " host=" + host + " port=" + port;
        PGconn *conn = PQconnectdb(connInfo.c_str());

        if (PQstatus(conn) != CONNECTION_OK) {
            LOG_ERROR << "Connessione al database " + std::string(dbName) + " fallita: " << PQerrorMessage(conn);
            PQfinish(conn);
            throw std::exception();
        }

        return conn;
    }

    static bool databaseExists(PGconn *conn, const char *dbName) {
        // Query per verificare se il database esiste già
        const char *checkDbExistsQuery = "SELECT 1 FROM pg_database WHERE datname = $1";

        // Parametri per la query
        const char *paramValues[1] = {dbName};
        int paramLengths[1] = {static_cast<int>(strlen(dbName))};
        int paramFormats[1] = {0};

        // Esegui la query per verificare se il database esiste già
        PGresult *res = PQexecParams(conn, checkDbExistsQuery, 1, nullptr, paramValues, paramLengths, paramFormats, 0);
        if (PQresultStatus(res) != PGRES_TUPLES_OK) {
            LOG_ERROR << "Query per verificare l'esistenza del database " << std::string(dbName) << "fallita: " << PQerrorMessage(conn);
            PQclear(res);
            PQfinish(conn);
            throw std::exception();
        }

        return PQntuples(res) > 0;
    }

    static void createDatabase(PGconn *conn, const char *dbName) {
        LOG_DEBUG << "DB " << std::string(dbName) << " does not exist, creating it...";
        std::string createDbQueryStr = "CREATE DATABASE " + std::string(dbName);
        const char *createDbQuery = createDbQueryStr.c_str();

        // Esegui la query per creare il database
        PGresult *res = PQexec(conn, createDbQuery);
        if (PQresultStatus(res) != PGRES_COMMAND_OK) {
            std::cerr << "Creazione del database fallita: " << PQerrorMessage(conn) << std::endl;
            PQclear(res);
            PQfinish(conn);
            throw std::exception();
        }
        PQclear(res);
        LOG_DEBUG << "Database " << std::string(dbName) << " created";
    }

    static void createTables(PGconn *conn) {
        LOG_DEBUG << "Creating tables...";
        if (drogon::app().getCustomConfig().isMember("db") && drogon::app().getCustomConfig()["db"].isMember("init_sql_path")) {
            std::string filePath = drogon::app().getCustomConfig()["db"]["init_sql_path"].asString();
            executeSqlFile(conn, filePath);

            LOG_DEBUG << "Tables created";
        } else {
            LOG_ERROR << "init_sql_path not found in config, could not create tables";
            throw std::runtime_error("init_sql_path not found in config, could not create tables");
        }
    }

    static void executeSqlCommand(PGconn *conn, const std::string &command) {
        LOG_DEBUG << "Executing SQL command: " << command;

        // Execute the SQL command
        PGresult *res = PQexec(conn, command.c_str());

        // Check if the SQL command was executed successfully
        if (PQresultStatus(res) != PGRES_COMMAND_OK) {
            std::cerr << "Execution of SQL command failed: " << PQerrorMessage(conn) << std::endl;
        }

        // Clear the result
        PQclear(res);
    }

    static void executeSqlFile(PGconn *conn, const std::string &filePath) {
        // Open the SQL file
        std::ifstream sqlFile(filePath);

        // Check if the file was opened successfully
        if (!sqlFile) {
            LOG_ERROR << "Could not open SQL file: " << filePath;
            throw std::exception();
        }

        // Read the file content into a string
        std::stringstream buffer;
        buffer << sqlFile.rdbuf();
        std::string sqlCommands = buffer.str();

        // Split the commands
        std::istringstream commandStream(sqlCommands);
        std::string command;
        while (std::getline(commandStream, command, ';')) {
            // Skip empty commands
            if (command.empty() || command == "\n" || command.find("--") == 0) {
                continue;
            }

            LOG_DEBUG_IF(SHOW_SQL) << "Executing SQL command: " << command;

            // Execute the SQL command
            PGresult *res = PQexec(conn, command.c_str());

            // Check if the SQL command was executed successfully
            if (PQresultStatus(res) != PGRES_COMMAND_OK) {
                LOG_ERROR << "Execution of SQL command failed: " << PQerrorMessage(conn);
            }

            // Clear the result
            PQclear(res);
        }
    }
};

#endif //ECOMMERCE_BE_DBCONNECTOR_H
