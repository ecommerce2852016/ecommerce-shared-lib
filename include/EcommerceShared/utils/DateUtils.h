//
// Created by federico on 04/03/24.
//

#ifndef ECOMMERCE_BE_DATEUTILS_H
#define ECOMMERCE_BE_DATEUTILS_H

#include <string>
#include <chrono>
#include <regex>

class DateUtils {
public:

    static constexpr const char *YYYY_MM_DD = R"(\b(\d{4})-(\d{2})-(\d{2})\b)";


    static std::string getCurrentDate() {
        auto now = std::chrono::system_clock::now();
        auto in_time_t = std::chrono::system_clock::to_time_t(now);
        std::string date = std::ctime(&in_time_t);
        date.pop_back();
        return date;
    }

    static std::string getTodayPlusDays(int days) {
        auto now = std::chrono::system_clock::now();
        auto in_time_t = std::chrono::system_clock::to_time_t(now);
        in_time_t += days * 24 * 60 * 60;
        std::string date = std::ctime(&in_time_t);
        date.pop_back();
        return date;
    }

    static std::string getYearFromDate(const std::string &input_date) {
        return getMatchGroup(input_date, YYYY_MM_DD, 1);
    }

    static std::string getMonthFromDate(const std::string &input_date) {
        return getMatchGroup(input_date, YYYY_MM_DD, 2);
    }

    static std::string getDayFromDate(const std::string &input_date) {
        return getMatchGroup(input_date, YYYY_MM_DD, 3);
    }

private:
    static std::string getMatchGroup(const std::string& input, const std::string &regex, const int group) {
        std::regex reg(regex);
        if (std::regex_match(input, reg)) {
            std::smatch match;
            std::regex_search(input, match, reg);
            return match[group].str();
        } else {
            throw std::invalid_argument("Invalid format");
        }
    }
};

#endif //ECOMMERCE_BE_DATEUTILS_H
