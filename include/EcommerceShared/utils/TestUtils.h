#ifndef ECOMMERCE_BE_TESTUTILS_H
#define ECOMMERCE_BE_TESTUTILS_H

#include <string>
#include <random>
#include <iostream>
#include "EcommerceShared/dto/user/SignupUserDTO.h"
#include "EcommerceShared/dto/payment/CreateNewStripeCustomerDTO.h"
#include "EcommerceShared/dto/payment/CreateOrUpdateStripeAccountDTO.h"
#include "EcommerceShared/dto/payment/PlaceOrderPaymentRequest.h"
#include "EcommerceShared/dto/product/InsertNewProductDTO.h"
#include "EcommerceShared/dto/product/SaveProductSupplyDTO.h"
#include "EcommerceShared/dto/user/UserAccountDTO.h"

using namespace ecommerce::dto;

class TestUtils {
public:

    static SignupUserAccountDTO randomSignupUserAccountDTO(AccountType accountType) {
        SignupUserAccountDTO request;
        request.accountType = accountType;
        request.email = TestUtils::randomEmail();
        request.password = TestUtils::randomSpecialAlphaNumericString(16);
        request.username = TestUtils::randomAlphanumericString(16);
        request.firstName = TestUtils::randomAlphabeticString(16);
        request.lastName = TestUtils::randomAlphabeticString(16);
        request.partitaIva = TestUtils::randomNumericString(11);
        request.taxCode = TestUtils::randomNumericString(16);
        request.ipAddress = TestUtils::randomIpAddress();
        request.fullName = request.firstName + " " + request.lastName;
        return request;
    }

    static CreateNewStripeCustomerDTO randomCreateNewStripeCustomerDTO() {
        CreateNewStripeCustomerDTO request;
        request.customerId = TestUtils::randomNumericLong();
        request.name = TestUtils::randomAlphabeticString(16);
        request.email = TestUtils::randomEmail();
        return request;
    }

    static CreateOrUpdateStripeAccountDTO randomCreateOrUpdateStripeAccountDTO() {
        CreateOrUpdateStripeAccountDTO request;
        request.userAccountId = TestUtils::randomNumericLong();
        request.email = TestUtils::randomEmail();
        request.firstName = TestUtils::randomAlphabeticString(16);
        request.lastName = TestUtils::randomAlphabeticString(16);
        request.fullName = request.firstName.value() + " " + request.lastName.value();
        request.birthDate = "1990-01-01";
        request.phone = TestUtils::randomNumericString(10);

        request.city = TestUtils::randomAlphabeticString(16);
        request.postalCode = TestUtils::randomNumericString(5);
        request.line = TestUtils::randomAlphabeticString(16);

        request.tosAcceptance = true;
        request.tosAcceptanceIpAddress = TestUtils::randomIpAddress();

        return request;
    }

    static PlaceOrderPaymentRequest randomPlaceOrderPaymentRequest() {
        PlaceOrderPaymentRequest request;
        request.orderId = TestUtils::randomNumericLong();
        request.customerId = TestUtils::randomNumericLong();

        long num_items = TestUtils::randomBetween(1, 30);
        for (int i = 0; i < num_items; i++) {
            request.items.push_back(randomCartItemDTO());
        }
        return request;
    }

    static CartItemDTO randomCartItemDTO() {
        CartItemDTO request;
        request.id = TestUtils::randomNumericLong();
        request.vendorId = TestUtils::randomNumericLong();
        request.quantityInsideCart = TestUtils::randomBetween(1, 10);
        request.quantityAvailable = request.quantityInsideCart + TestUtils::randomBetween(1, 10);

        request.pricePerItem = TestUtils::randomFloatBetween(1, 100);
        request.priceTotal = request.pricePerItem * request.quantityInsideCart;

        request.supplyId = TestUtils::randomNumericLong();
        request.productId = TestUtils::randomNumericLong();
        request.productName = TestUtils::randomAlphabeticString(16);
        request.productWeight = TestUtils::randomFloatBetween(1, 10);
        request.fromAddressId = TestUtils::randomNumericLong();

        return request;
    }

    static SaveProductSupplyDTO randomSaveProductSupplyDTO() {
        SaveProductSupplyDTO request;
        request.productId = TestUtils::randomNumericLong();
        request.quantityVariation = TestUtils::randomBetween(1, 5);
        request.addressId = TestUtils::randomNumericLong();
        request.price = TestUtils::randomFloatBetween(1, 1000);
        return request;
    }

    static UserAccountDTO randomUserAccountDTO(AccountType accountType) {
        UserAccountDTO user;
        user.id = randomNumericLong();
        user.username = randomAlphabeticString(16);
        user.email = randomEmail();
        user.createdAt = "2021-01-01";
        user.accountType = accountType;
        user.taxCode = randomNumericString(16);
        user.partitaIva = randomNumericString(11);
        user.firstName = randomAlphabeticString(16);
        user.lastName = randomAlphabeticString(16);
        user.fullName = user.firstName + " " + user.lastName;
        return user;
    }

    static InsertNewProductDTO randomInsertNewProductDTO() {
        InsertNewProductDTO request;
        request.upcCode = TestUtils::randomAlphanumericString(16);
        request.name = TestUtils::randomAlphabeticString(16);
        request.description = TestUtils::randomAlphabeticString(32);
        request.category = randomProductCategory();
        request.weight = TestUtils::randomFloatBetween(1, 10);
        request.length = TestUtils::randomFloatBetween(1, 10);
        request.width = TestUtils::randomFloatBetween(1, 10);
        request.height = TestUtils::randomFloatBetween(1, 10);

        return request;
    }

    static ProductCategory randomProductCategory() {
        return static_cast<ProductCategory>(randomBetween(0, 34));
    }

    static std::string randomAlphabeticString(int length) {
        const char alphanum[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        return generateRandomString(alphanum, sizeof(alphanum) - 1, length);
    }

    static std::string randomAlphanumericString(int length) {
        const char alphanum[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";
        return generateRandomString(alphanum, sizeof(alphanum) - 1, length);
    }

    static std::string randomSpecialAlphaNumericString(int length) {
        const char alphanum[] = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz!@#$%^&*()_+";
        return generateRandomString(alphanum, sizeof(alphanum) - 1, length);
    }

    static std::string randomNumericString(int length) {
        const char alphanum[] = "0123456789";
        return generateRandomString(alphanum, sizeof(alphanum) - 1, length);
    }

    static long randomNumericLong() {
        return std::stol(randomNumericString(16));
    }

    static int randomNumericInt() {
        return std::stoi(randomNumericString(8));
    }

    static int randomNumericIntBetween(int min, int max) {
        return randomBetween(min, max);
    }

    static double randomNumericDouble() {
        return std::stod(randomNumericString(8) + "." + randomNumericString(2));
    }

    static long randomBetween(int min, int max) {
        static std::random_device rd;
        static std::mt19937 gen(rd()); // Mersenne Twister 19937 a 32 bit engine
        std::uniform_int_distribution<> dis(min, max);
        return dis(gen);
    }

    static float randomFloatBetween(float min, float max) {
        static std::random_device rd;
        static std::mt19937 gen(rd()); // Mersenne Twister 19937 a 32 bit engine
        std::uniform_real_distribution<float> dis(min, max);
        return dis(gen);
    }

    static std::string randomEmail() {
        return randomAlphabeticString(16) + "@mailinator.com";
    }

    static std::string randomIpAddress() {
        return randomNumericString(3) + "." + randomNumericString(3) + "." + randomNumericString(3) + "." + randomNumericString(3);
    }

private:
    static std::string generateRandomString(const char *characters, size_t length, int stringLength) {
        static std::random_device rd;
        static std::mt19937 gen(rd()); // Mersenne Twister 19937 a 32 bit engine
        std::uniform_int_distribution<> dis(0, length - 1);

        std::string s;
        for (int i = 0; i < stringLength; ++i) {
            s += characters[dis(gen)];
        }
        return s;
    }
};

#endif //ECOMMERCE_BE_TESTUTILS_H
