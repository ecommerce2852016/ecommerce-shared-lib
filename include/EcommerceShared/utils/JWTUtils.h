//
// Created by federico on 21/02/24.
//

#ifndef ECOMMERCE_BE_JWTUTILS_H
#define ECOMMERCE_BE_JWTUTILS_H

#include <jwt-cpp/jwt.h>
#include "EcommerceShared/dto/user/UserAccountDTO.h"
#include "json_struct.h"
#include "StringUtils.h"

class JWTUtils {
public:
    static std::string generateJWT(const ecommerce::dto::UserAccountDTO &userAccountDto, const std::chrono::system_clock::time_point &expiresAt) {

        auto userAccountJsonString = JS::serializeStruct(userAccountDto, JS::SerializerOptions(JS::SerializerOptions::Compact));

        auto token = jwt::create()
                .set_issuer("ecommerce-be")
                .set_type("JWS")
                .set_payload_claim("user_account", jwt::claim(userAccountJsonString))
                .set_issued_at(std::chrono::system_clock::now())
                .set_expires_at(expiresAt)
                .sign(jwt::algorithm::hs512(secret));

        return token;
    }

    static bool isTokenExpired(const std::string &token) {
        auto decodedToken = jwt::decode(token);
        auto expirationTime = decodedToken.get_expires_at();
        return std::chrono::system_clock::now() > expirationTime;
    }

    static ecommerce::dto::UserAccountDTO getUserAccountFromJWT(const std::string &token) {
        auto decodedToken = jwt::decode(token);
        auto userAccountJsonString = decodedToken.get_payload_claim("user_account").as_string();
        return StringUtils::parseJsonString<ecommerce::dto::UserAccountDTO>(userAccountJsonString);
    }

    static AccountType getAccountTypeFromJWT(const std::string &token) {
        auto userAccount = getUserAccountFromJWT(token);
        return userAccount.accountType;
    }

private:
    static constexpr auto secret = "DlIjG/MAnJMQ+sIJBbZcNasFOBw2Pup7CrtOyTjnGgh8AWZLaxLqgVxf8ljl3C0d3mzoepIy7ljIXk0eo+PTGKNBQ0VGa7AW8L/qPZ3qVhE=";
};


#endif //ECOMMERCE_BE_JWTUTILS_H
