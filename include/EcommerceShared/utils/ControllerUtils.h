//
// Created by federico on 14/02/24.
//

#ifndef ECOMMERCE_BE_CONTROLLERUTILS_H
#define ECOMMERCE_BE_CONTROLLERUTILS_H

#include <drogon/HttpController.h>
#include <drogon/HttpResponse.h>
#include <json/json.h>
#include "StringUtils.h"
#include "JWTUtils.h"
#include "EcommerceShared/exceptions/http_exception.h"

using namespace drogon;
using namespace drogon::orm;

class ControllerUtils {

public:
    static std::shared_ptr<drogon::HttpResponse> newJsonResponse(const Json::Value &json, HttpStatusCode status) {
        auto response = HttpResponse::newHttpJsonResponse(json);
        response->setStatusCode(status);
        response->setContentTypeCode(drogon::ContentType::CT_APPLICATION_JSON);
        response->setBody(json.toStyledString());

        return response;
    }

    static std::shared_ptr<drogon::HttpResponse> okJsonResponse(const Json::Value &json) {
        return ControllerUtils::newJsonResponse(json, HttpStatusCode::k200OK);
    }

    static std::shared_ptr<drogon::HttpResponse> errorJsonResponse(const std::string &message, HttpStatusCode httpStatusCode) {
        Json::Value json = simpleJsonResponse(message, httpStatusCode);

        auto response = HttpResponse::newHttpJsonResponse(json);
        response->setStatusCode(httpStatusCode);
        response->setContentTypeCode(drogon::ContentType::CT_APPLICATION_JSON);
        response->setBody(json.toStyledString());

        return response;
    }

    static std::shared_ptr<drogon::HttpResponse> okResponse(const std::string &message) {
        Json::Value json = simpleJsonResponse(message, HttpStatusCode::k200OK);

        auto response = HttpResponse::newHttpJsonResponse(json);
        response->setStatusCode(k200OK);
        response->setContentTypeCode(drogon::ContentType::CT_APPLICATION_JSON);
        response->setBody(json.toStyledString());

        return response;
    }

    template<typename T>
    static std::shared_ptr<drogon::HttpResponse> okStructResponse(const T &serializableStruct) {
        auto jsonString = JS::serializeStruct(serializableStruct, JS::SerializerOptions(JS::SerializerOptions::Compact));
        return okJsonResponse(StringUtils::parseJson(jsonString));
    }

    static Json::Value simpleJsonResponse(const std::string &message, HttpStatusCode httpStatusCode) {
        Json::Value json;
        json["status"] = httpStatusCode;
        json["message"] = message;

        return json;
    }


    template<typename T>
    static T parseRequestBody(const HttpRequest &request) {
        auto jsonString = string(request.getBody());
        return StringUtils::parseJsonString<T>(jsonString);
    }


    static string getTokenFromRequest(const HttpRequestPtr &req) {
        auto token_header = req->getHeader("Authorization");
        if (token_header.empty()) {
            return "";
        }
        return StringUtils::splitString(token_header, ' ')[1];
    }

    /**
     * @return the user account extrated from token in the request as header "Authorization"
     * @throws http_exception 401 if no token is found in the request
     */
    static ecommerce::dto::UserAccountDTO getUserAccountFromRequestToken(const HttpRequestPtr &req) {
        auto token = getTokenFromRequest(req);
        if (token.empty()) {
            throw ecommerce::exceptions::http_exception("No token found in request", HttpStatusCode::k401Unauthorized);
        }
        return JWTUtils::getUserAccountFromJWT(ControllerUtils::getTokenFromRequest(req));
    }


    static optional<map<string, string>> queryParamFromString(const std::string &queryParam) {
        if (!queryParam.empty()) {
            // convert query param to map
            auto queryParamMap = StringUtils::splitString(queryParam, '&');
            map<string, string> queryParamMapResult;
            for (const auto &param: queryParamMap) {
                auto keyValue = StringUtils::splitString(param, '=');
                if (keyValue.size() == 2) {
                    queryParamMapResult.insert({keyValue[0], keyValue[1]});
                }
            }
            return queryParamMapResult;
        }
        return nullopt;
    }

    static std::string getClientIpAddress(const HttpRequestPtr &req) {
        auto ip = req->getHeader("X-Real-IP");
        if (ip.empty()) {
            ip = req->getHeader("X-Forwarded-For");
        }
        if (ip.empty()) {
            ip = req->getPeerAddr().toIp();
        }
        return ip;
    }

    static bool isSuccessStatusCode(const HttpStatusCode &status) {
        return status >= HttpStatusCode::k200OK && status < HttpStatusCode::k300MultipleChoices;
    }


private:

};

#endif //ECOMMERCE_BE_CONTROLLERUTILS_H
