
#ifndef ECOMMERCE_BE_ECOMMERCECONFIGLOADER_H
#define ECOMMERCE_BE_ECOMMERCECONFIGLOADER_H

#include <string>
#include <json/value.h>
#include <drogon/HttpAppFramework.h>
#include <drogon/HttpController.h>
#include <postgresql/libpq-fe.h>
#include <iostream>
#include <fstream>
#include <json/json.h>
#include "JsonUtils.h"
#include <getopt.h>

class EcommerceConfigLoader {
public:

    inline static std::string configPath = "../config.json";


    static void loadArgsOpt(int argc, char *argv[]) {
        int option_index = 0;
        EcommerceConfigLoader::configPath = defaultConfigPath();


        // Definizione delle opzioni lunghe
        static struct option long_options[] = {
                {"config", optional_argument, 0, 'c'},
                {0,        0,                 0, 0}
        };

        int c;
        while ((c = getopt_long(argc, argv, "c:", long_options, &option_index)) != -1) {
            switch (c) {
                case 'c':
                    configPath = optarg;
                    break;
                case '?':
                    std::cerr << "Uso: " << argv[0] << " --config=/config_path.json\n";
                    throw std::invalid_argument("Invalid arguments");
                default:
                    std::cerr << "Opzione non riconosciuta\n";
                    throw std::invalid_argument("Invalid arguments");
            }
        }

        LOG_DEBUG << "Loading config from " << configPath;
        // get the json config object
        auto config = EcommerceConfigLoader::getConfig();
        auto redis_host = getenv("REDIS_HOST");
        if (redis_host != nullptr) config["redis_client"][0]["host"] = redis_host;
        drogon::app().loadConfigJson(config);
    }

    static Json::Value getConfig() {
        return JsonUtils::readJson(EcommerceConfigLoader::configPath);
    }

    static bool isTestEnvironment() {
        auto ENVIRONMENT = getenv("ENVIRONMENT");
        return ENVIRONMENT != nullptr && std::string(ENVIRONMENT) == "test";
    }

    static void setLogLevel(trantor::Logger::LogLevel level) {
        drogon::app().setLogLevel(level);
    }

    static std::string defaultConfigPath() {
        if (isTestEnvironment()) {
            return "../../config_test.json";
        } else {
            return "../config.json";
        }
    }

    static std::string getDbName(int clientIndex = 0) {
        return getConfig()["db_clients"][clientIndex]["dbname"].asString();
    }

    static bool shouldClearOnShutdown() {
        auto dbConfig = drogon::app().getCustomConfig()["db"];
        bool shouldClear = dbConfig.isMember("clear_on_shutdown") && dbConfig["clear_on_shutdown"].asBool();
        return shouldClear;
    }

private:

};

#endif //ECOMMERCE_BE_ECOMMERCECONFIGLOADER_H
