//
// Created by federico on 13/02/24.
//

#ifndef ECOMMERCE_BE_STRINGUTILS_H
#define ECOMMERCE_BE_STRINGUTILS_H


#include <string>
#include <drogon/drogon.h>
#include "json_struct.h"
#include "EcommerceShared/exceptions/http_exception.h"

using namespace std;


class StringUtils {
public:
    static std::vector<std::string> splitString(std::string str, char splitter) {
        std::vector<std::string> result;
        std::string current = "";
        for (int i = 0; i < str.size(); i++) {
            if (str[i] == splitter) {
                if (current != "") {
                    result.push_back(current);
                    current = "";
                }
                continue;
            }
            current += str[i];
        }
        if (current.size() != 0)
            result.push_back(current);
        return result;
    };

    static std::string removeFirstAndLastChar(std::string str) {
        return str.substr(1, str.size() - 2);
    }

    static std::string join(const std::vector<std::string> &strings, const char joiner) {
        std::string result = "";
        for (int i = 0; i < strings.size(); i++) {
            result += strings[i];
            if (i != strings.size() - 1) {
                result += joiner;
            }
        }
        return result;
    }

    static string replaceAll(string str, const string &from, const string &to) {
        size_t start_pos = 0;
        while ((start_pos = str.find(from, start_pos)) != string::npos) {
            str.replace(start_pos, from.length(), to);
            start_pos += to.length(); // In case 'to' contains 'from', like replacing 'x' with 'yx'
        }
        return str;
    }


    template<typename T>
    static T parseJsonString(const std::string &s) {
        T obj;
        JS::ParseContext parseContext(s);
        if (parseContext.parseTo(obj) != JS::Error::NoError) {
            LOG_ERROR << "Failed to parse JSON: " << parseContext.makeErrorString();
            throw ecommerce::exceptions::http_exception("Failed to parse JSON: " + parseContext.makeErrorString(), drogon::k500InternalServerError);
        };
        return obj;
    }

    static Json::Value parseJson(const std::string &s) {
        Json::Value obj;
        Json::CharReaderBuilder builder;
        std::unique_ptr<Json::CharReader> reader(builder.newCharReader());
        JSONCPP_STRING errs;
        if (!reader->parse(s.c_str(), s.c_str() + s.length(), &obj, &errs)) {
            LOG_ERROR << "Failed to parse JSON: " << errs;
            throw ecommerce::exceptions::http_exception("Failed to parse JSON: " + errs, drogon::k500InternalServerError);
        }
        return obj;
    }

};

#endif //ECOMMERCE_BE_STRINGUTILS_H
