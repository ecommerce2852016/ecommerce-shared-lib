//
// Created by federico on 07/03/24.
//

#ifndef ECOMMERCE_BE_VECTORUTILS_H
#define ECOMMERCE_BE_VECTORUTILS_H

#include <iostream>
#include <vector>
#include <algorithm>
#include <numeric>

class VectorUtils {
public:
    template<typename T, typename F>
    static auto map(const std::vector<T> &vec, F &&func) {
        std::vector<decltype(func(std::declval<T>()))> result;
        result.reserve(vec.size());
        std::transform(vec.begin(), vec.end(), std::back_inserter(result), std::forward<F>(func));
        return result;
    }

    template<typename T>
    static T min_element(const std::vector<T> &vec, const T &default_value) {
        if (vec.empty()) {
            return default_value;
        }
        // Trova l'elemento minimo nel vettore
        return *std::min_element(vec.begin(), vec.end());
    }

    template<typename T>
    static T max_element(const std::vector<T> &vec, const T &default_value) {
        if (vec.empty()) {
            return default_value;
        }
        // Trova l'elemento minimo nel vettore
        return *std::max_element(vec.begin(), vec.end());
    }

    template<typename T>
    static T sum(const std::vector<T> &vec, const T &default_value) {
        if (vec.empty()) {
            return default_value;
        }
        // Somma tutti gli elementi del vettore
        return std::accumulate(vec.begin(), vec.end(), T());
    }

    template<typename T, typename Predicate>
    static std::vector<T> filter(const std::vector<T> &vec, Predicate pred) {
        std::vector<T> result;
        std::copy_if(vec.begin(), vec.end(), std::back_inserter(result), pred);
        return result;
    }


    template<typename T>
    static std::vector<T> filterUnique(const std::vector<T> &inputVector, std::function<bool(const T &, const T &)> equalityFunction) {
        std::vector<T> uniqueVector;
        for (const auto &item: inputVector) {
            bool isUnique = std::none_of(uniqueVector.begin(), uniqueVector.end(), [&](const T &elem) { return equalityFunction(elem, item); });
            if (isUnique) {
                uniqueVector.push_back(item);
            }
        }
        return uniqueVector;
    }

    template<typename T>
    static bool contains(const std::vector<T> &vec, const T &element) {
        return std::find(vec.begin(), vec.end(), element) != vec.end();
    }


};


#endif //ECOMMERCE_BE_VECTORUTILS_H
