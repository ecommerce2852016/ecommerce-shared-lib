#ifndef ECOMMERCE_BE_JSONUTILS_H
#define ECOMMERCE_BE_JSONUTILS_H


#include <string>
#include <json/value.h>
#include <iostream>
#include <fstream>
#include <json/reader.h>

class JsonUtils {
public:
    static Json::Value readJson(const std::string &filePath) {
        // Crea un oggetto ifstream per leggere il file JSON
        std::ifstream fileStream(filePath);
        if (!fileStream.is_open()) {
            std::cerr << "Impossibile aprire il file JSON" << std::endl;
            return 1;
        }

        // Crea un oggetto Json::Value per contenere il contenuto JSON
        Json::Value root;

        // Crea un oggetto Json::Reader per analizzare il contenuto JSON
        Json::Reader reader;

        // Leggi il file JSON nel Json::Value
        bool parsingSuccessful = reader.parse(fileStream, root);
        if (!parsingSuccessful) {
            // La lettura del file JSON ha fallito, stampa l'errore
            std::cerr << "Impossibile analizzare il file JSON: " << reader.getFormattedErrorMessages() << std::endl;
            return 1;
        }
        return root;
    }
};

#endif //ECOMMERCE_BE_JSONUTILS_H
